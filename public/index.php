<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/* Load all libraries installed via Composer */
require '../vendor/autoload.php';

/* Prepares all configuration files */
$config = array();
foreach (glob('../app/config/*.php') as $configFile) {
    require $configFile;
}

$app = new \Slim\App($config);

require '../app/container/container.php';
require '../app/routes/InitialRoute.php';

//Using this middleware to remove trailing slash
$app->add(function ($request, $response, $next) {
    $uri = $request->getUri();
    $path = $uri->getPath();
    if ($path != '/' && substr($path, -1) == '/') {
        // permanently redirect paths with a trailing slash
        // to their non-trailing counterpart
        $uri = $uri->withPath(substr($path, 0, -1));
        
        if($request->getMethod() == 'GET') {
            return $response->withRedirect((string)$uri, 301);
        }
        else {
            return $next($request->withUri($uri), $response);
        }
    }

    return $next($request, $response);
});
$app->run();
