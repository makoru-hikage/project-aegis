
var ft = null;
var $editor = $('#editor');
var $modal = $('#editor-modal');
var $editorTitle = $('#editor-title');

    $.ajax({
        url : '/records/'+global.entityName,
        method: 'GET'
    }).done(function (response){
    ft = FooTable.init('#crud-datatable', {
        showHeader: true,
        columns : global.entity_fields,
        rows: response.data,
        editing: {
            enabled: true,
            editRow: function(row){
                $editor.attr('method', 'PUT');
                var values = row.val();
                for(v in values){
                    $editor.find('#'+global.entityName+'_form_'+v).val(values[v]);
                };
                $modal.data('row', row);
                $editorTitle.text('Edit row #' + values.id);
                $modal.modal('show');
            },
            deleteRow: function(row){
                var id = row.val()['id'];
                if (confirm('Are you sure you want to delete the row?')){
                    $.ajax({
                        url: '/records/'+global.entityName+'/'+id,
                        method: 'DELETE'
                    }).done( function (data){
                        row.delete();
                    });  
                }
            },
            alwaysShow: true,
            allowAdd: false
        }
    });
});

function addRow(e){
    $editor.attr('method', 'POST');
    $modal.removeData('row');
    $editor[0].reset();
    $editorTitle.text('Add a new row');
    $modal.modal('show');
}

$('.footable-add').on('click', addRow);


$editor.on('submit', function(e){
    
        e.preventDefault();
        var method = $editor.attr('method'),
            formData = $editor.serializeArray(),
            data = {}
            row = $modal.data('row');
            

        formData.forEach(function (field, index, form){
            //Should the name of the input be like this "entity_form[field]",
            //extract the "field".
            var regexExtract = field.name.match(/.*\[([A-Za-z_]+)\]/);
            var attrib = regexExtract ? regexExtract[1] : attrib;
            data[attrib] = field.value;
        });
        
        $.ajax({
            data: data,
            url: '/records/'+global.entityName+'/'+data.id,
            method: method
        });

        if (row instanceof FooTable.Row){
            row.val(data);
        } else {
            ft.rows.add(data);
        }
        $modal.modal('hide');
    });




