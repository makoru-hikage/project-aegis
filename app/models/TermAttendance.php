<?php

namespace DeltaX\Models;
use Illuminate\Database\Eloquent\Model;


class TermAttendance extends Model {

	use \DeltaX\Crud\Traits\AlternateIdTrait;
        
	protected $table = 'term_attendance';
	protected $alternateIdKey = 'code';

	protected $hidden = ['created_at', 'updated_at', 'is_deleted'];
	protected $guarded = ['id','created_at', 'updated_at'];
	
	public function student(){
		return $this->belongsTo('\DeltaX\Models\Student', 'student_id');
	}

	public function term(){
		return $this->belongsTo('\DeltaX\Models\SchoolCalendar', 'school_calendar_id');
	}
}