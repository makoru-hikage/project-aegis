<?php

namespace DeltaX\Models;
use Illuminate\Database\Eloquent\Model;

class Remark extends Model{
	use \DeltaX\Crud\Traits\AlternateIdTrait;
	protected $alternateIdKey = 'remark';
}