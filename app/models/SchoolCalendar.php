<?php

namespace DeltaX\Models;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class SchoolCalendar extends Model {

    protected $guarded = ['id','created_at', 'updated_at'];
    protected $hidden = ['created_at', 'updated_at', 'is_deleted'];
    /*protected $appends = ['academic_year'];*/

    protected $table = 'school_calendar';
    protected $alternateIdKey = 'code';

    public function sessionsDuringThis(){
        return $this->hasMany('\DeltaX\Models\CourseSession');
    }

}