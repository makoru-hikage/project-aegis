<?php

namespace DeltaX\Models;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model {

	use \DeltaX\Crud\Traits\AlternateIdTrait;
	protected $alternateIdKey = 'employee_number';

	protected $guarded = ['id','created_at', 'updated_at'];
	protected $hidden = ['created_at', 'updated_at', 'is_deleted'];

	public function teaches(){
		return $this->hasManyThrough('\DeltaX\Models\Course','\DeltaX\Models\CourseSession');
	}
	
	public function userInfo(){
		return $this->belongsTo('\DeltaX\Models\User', 'user_id');
	}
}