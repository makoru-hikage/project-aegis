<?php

namespace DeltaX\Models;
use Illuminate\Database\Eloquent\Model;

class Degree extends Model {

	use \DeltaX\Crud\Traits\AlternateIdTrait;
	protected $alternateIdKey = 'code';

	protected $hidden = ['created_at', 'updated_at', 'is_deleted'];
	protected $guarded = ['id','created_at', 'updated_at'];
	
	public function curriculum(){
		return $this->hasManyThrough(
			'\DeltaX\Models\Curriculum', 
			'\DeltaX\Models\CurriculumTerm',
			'degree_id',
			'curriculum_term_id'
		)->select('curricula.id', 'course_id', 'lec_contact_hrs', 'lab_contact_hrs');
	}

	public function students(){
		return $this->hasMany('\DeltaX\Models\Students');
	}

	public function getNameAndCodeAttribute(){
		return $this->name . ' (' . $this->code .')';
	}

	public function terms()
	{
		return $this->hasMany('\DeltaX\Models\CurriculumTerms');
	}
	
}