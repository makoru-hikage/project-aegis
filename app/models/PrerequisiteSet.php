<?php

namespace DeltaX\Models;
use Illuminate\Database\Eloquent\Model;

class PrerequisiteSet extends Model {

	protected $guarded = ['id','created_at', 'updated_at'];
	protected $hidden = ['created_at', 'updated_at', 'is_deleted'];

	public function coursesList(){
		return $this->belongsToMany( 
			'\DeltaX\Models\Course',
			'prerequisite_courses',
			'prerequisite_set_id',
			'course_id'
		)->select('courses.id','code', 'name');
	}

	public function curriculum(){
		return $this->belongsTo('\DeltaX\Models\Curriculum', 'curriculum_id');
	}

}