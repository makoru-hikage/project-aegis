<?php

namespace DeltaX\Models;
use Illuminate\Database\Eloquent\Model;

class Curriculum extends Model {

	protected $table = 'curricula';
	protected $guarded = ['id','created_at', 'updated_at'];
	protected $hidden = ['created_at', 'updated_at', 'is_deleted'];

	
	public function course(){
		return $this->belongsTo('\DeltaX\Models\Course', 'course_id');
	}

	public function prerequisite(){
		return $this->belongsToMany('\DeltaX\Models\Curriculum', 'curriculum_prerequisites', 'dependent_id', 'prerequisite_id');
	}

	public function curriculum_term(){
		return $this->belongsTo('\DeltaX\Models\CurriculumTerm', 'curriculum_term_id');
	}
}