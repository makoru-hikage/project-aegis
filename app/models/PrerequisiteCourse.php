<?php

namespace DeltaX\Models;
use Illuminate\Database\Eloquent\Model;

class PrerequisiteCourse extends Model {

	protected $guarded = ['id','created_at', 'updated_at'];
	protected $hidden = ['created_at', 'updated_at', 'is_deleted'];

	public function course(){
		return $this->hasOne('\DeltaX\Models\Course', 'course_id')->select('id', 'code', 'name');
	}

	public function prerequisiteSet(){
		return $this->belongsTo('\DeltaX\Models\PrerequisiteSet', 'prerequisite_set_id')
			->select('id', 'prerequisite_set_id','course_id');
	}

}