<?php

namespace DeltaX\Models;
use Illuminate\Database\Eloquent\Model;
use DeltaX\Models\{Student, CourseSession, Curriculum, Remark};

class EnrolledCourse extends Model {

	protected $table = "enrollment";

	protected $guarded = ['id','created_at', 'updated_at'];
	protected $hidden = ['created_at', 'updated_at', 'is_deleted'];

	public function setStudentNumberAttribute($value){
		$this->student_id = Student::where('student_number', $value)->first()->id;
		$this->save();
	}

	public function enrolledStudent(){
		return $this->belongsTo('\DeltaX\Models\Student', 'student_id')
			->select('id','student_number', 'user_id', 'degree_id', 'year_level', 'status');
	}

	public function courseSession(){
		return $this->belongsTo('\DeltaX\Models\CourseSession','course_session_id');
	}

	public function curriculum(){
		return $this->belongsTo('\DeltaX\Models\Curriculum','curriculum_id');
	}

	public function remark(){
		return $this->belongsTo('\DeltaX\Models\Remark','remark_id')->select('id', 'remark');
	}

}