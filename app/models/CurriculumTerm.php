<?php

namespace DeltaX\Models;
use Illuminate\Database\Eloquent\Model;


class CurriculumTerm extends Model {

	protected $guarded = ['id','created_at', 'updated_at'];
	protected $hidden = ['created_at', 'updated_at', 'is_deleted'];


    public function searchByDegreeAndSemester($degree_code, $year_level = null, $semester = null){
    	$q = new self;
    	$q = $degree_code ? $q->where('degree_id', Degree::where('code', $degree_code)->first()->id) : null;
    	$q = $year_level ? $q->where('year_level', $year_level) : $q;
    	$q = $semester ? $q->where('semester', $semester) : $q;

    	return $q;
    }

    public function getCurriculumItems(){
        return $this->hasMany('\DeltaX\Models\Curriculum', 'curriculum_id');
    }

}