<?php

namespace DeltaX\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;


class CurrentUserType extends Model{
	
	protected $primaryKey = 'user_id';

	public function info(){	
    
		Relation::morphMap([
			'student' => '\DeltaX\Models\Student',
			'employee' => '\DeltaX\Models\Employee'
		]);
    	return $this->morphTo();
    }

    public function user(){
    	return $this->belongsTo('\DeltaX\Models\User', 'user_id');
    }

}