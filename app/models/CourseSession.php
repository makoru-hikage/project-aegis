<?php

namespace DeltaX\Models;
use Illuminate\Database\Eloquent\Model;

class CourseSession extends Model {

	protected $guarded = ['id','created_at', 'updated_at'];
	protected $hidden = ['created_at', 'updated_at', 'is_deleted'];
	
	public function courseTaught(){
		return $this->belongsTo(Course::class, 'course_id');
	}

	public function taughtBy(){
		return $this->belongsTo(Employee::class, 'employee_id');
	}

	public function runDuring(){
		return $this->belongsTo(SchoolCalendar::class, 'school_calendar_id');
	}

}