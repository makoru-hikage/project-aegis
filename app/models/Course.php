<?php

namespace DeltaX\Models;
use Illuminate\Database\Eloquent\Model;

class Course extends Model {

	use \DeltaX\Crud\Traits\AlternateIdTrait;
	protected $alternateIdKey = 'code';

	protected $guarded = ['id','created_at', 'updated_at'];	
	protected $hidden = ['created_at', 'updated_at', 'is_deleted'];	
	
	public function sessions(){
		return $this->hasMany('\DeltaX\Models\CourseSession', 'course_session_id');
	}

	public function curriculum(){
		return $this->hasMany('\DeltaX\Models\Curriculum', 'curriculum_id');
	}

	public function isNeededBy(){
		return $this->hasManyThrough(
			'\DeltaX\Models\PrerequisiteSet',
			'\DeltaX\Models\PrerequisiteCourses',
			'prerequisite_course_id',
			'prerequisite_set_id'
		);
	}


}