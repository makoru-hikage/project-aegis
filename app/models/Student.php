<?php

namespace DeltaX\Models;
use Illuminate\Database\Eloquent\Model;

class Student extends Model {

    use \DeltaX\Crud\Traits\AlternateIdTrait;
    protected $alternateIdKey = 'student_number';

	protected $guarded = ['id','created_at', 'updated_at'];
	protected $hidden = ['created_at', 'updated_at', 'is_deleted'];
	protected $with = ['userInfo', 'degree'];

	public function degree(){
		return $this->belongsTo('\DeltaX\Models\Degree', 'degree_id');
	}

	public function userInfo(){
		return $this->belongsTo('\DeltaX\Models\User', 'user_id');
	}

	public function termsAttended(){
		return $this->hasMany('\DeltaX\Models\TermAttendance');
	}

	public function currentInfo(){
        return $this->morphOne('\DeltaX\Models\CurrentUserType', 'info');
    }

    public function enrollment(){
        return $this->hasMany('\DeltaX\Models\Enrollment');
    }

}