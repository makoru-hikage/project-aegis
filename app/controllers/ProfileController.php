<?php

namespace DeltaX\Controllers;

use DeltaX\FrontEndController\FrontEndController;
use Interop\Container\ContainerInterface;
use DeltaX\Crud\RepositoryInvoker;
use DeltaX\Repositories\CentralRepository;
use DeltaX\TableCreation\DataTableGenerator;
use Illuminate\Support\Str;

class ProfileController extends FrontEndController {

        protected $repoInvoker;

        public function __construct(ContainerInterface $c){
                parent::__construct($c);
                $this->repoInvoker = new RepositoryInvoker(new CentralRepository);
        }
	
	public function index($request, $response, $arguments){

                //Get the logged in user's model
                $user = $this->app->authentication_adapter->getUser();
                //Should the URL contain no 'id', load the user's profile id.
                $id = $arguments['id'] ?? $user->id;
                //Load the user information
                $user = $this->repoInvoker->find('users', $id);
                $this->data['user_info'] = $user;
                $this->data['user_type'] = $user->userType->info_type;
                $this->data['title'] = $user->full_name;

                $this->data['additional_info'] = $user->userType->info;
                
                
                $this->loadCss('vendor/footable/footable.bootstrap.css');
                $this->loadJs('vendor/footable/footable.min.js');
                $this->loadJs('app/test.js');
                
                $this->app->view->render($response,'profile/index.twig', $this->data);

	}
} 