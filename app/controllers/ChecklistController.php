<?php

namespace DeltaX\Aegis\Controllers;

use DeltaX\Repositories\CentralRepository;
use DeltaX\Aegis\ValidationRules\ChecklistSearchValidation;	

use DeltaX\Aegis\ModelServices\GradesRegistry\BlankChecklist;
use DeltaX\Aegis\ModelServices\GradesRegistry\StudentChecklist;

use DeltaX\Aegis\ModelServices\UserManagement\StudentIdentity;
use DeltaX\Crud\MenuService\MultipleReadModule;

use DeltaX\ApiController\ApiController;


class ChecklistController extends ApiController {
	
	public function getCurriculum($request, $response, $urlParams){

		$input = $request->getQueryParams();
		$input['degree_code'] = $urlParams['degree_code'];

		$repo = new CentralRepository;

		$output = (new MultipleReadModule ($input))
			->setModelService(new BlankChecklist($repo))
			->prepareValidation(new ChecklistSearchValidation)
			->run();

		return $response->withJson($output['data'], $output['code']);
	}

	public function getChecklist($request, $response, $urlParams){

		$input = $request->getQueryParams();
		$input['student_number'] = $urlParams['student_number'];

		$repo = new CentralRepository;

		$userIdentity = new StudentIdentity(
			$this->app->auth, 
			$repo,
			$input['student_number']
		);

		$output = (new MultipleReadModule ($input))
			->setModelService(new StudentChecklist($repo))
			->prepareAuthorization($userIdentity)
			->prepareValidation(new ChecklistSearchValidation)
			->run();

		return $response->withJson($output['data'], $output['code']);
	}

}