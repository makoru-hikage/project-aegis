<?php

namespace \DeltaX\Controllers;

use DeltaX\FrontEndController\FrontEndController;
use Interop\Container\ContainerInterface;
use DeltaX\Crud\RepositoryInvoker;
use DeltaX\Repositories\CentralRepository;

class AccountRegistrationController extends FrontEndController {
	
	protected $repoInvoker;

	public function __construct(ContainerInterface $c){
		parent::__construct($c);
		$this->repoInvoker = new RepositoryInvoker(new CentralRepository);
	}

	public function index($request, $response, $arguments){

		$studentFormClass = '\DeltaX\Forms\StudentForm';
		$userFormClass = '\DeltaX\Forms\UserForm';
		$this->data['user_form'] = $this->app->form_factory->create($UserFormClass)->createView();
		$this->data['student_form'] = $this->app->form_factory->create($StudentFormClass)->createView();
	}

	public function registerStudentUser(array $input){
						
	}
}