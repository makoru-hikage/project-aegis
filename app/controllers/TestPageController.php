<?php

namespace DeltaX\Controllers;

use DeltaX\FrontEndController\FrontEndController;


class TestPageController extends FrontEndController {

	
	public function index($request, $response, $arguments){

		$this->data['title'] = 'Test';

                
                $this->loadJs('vendor/lz-string/lz-string.min.js');
                $this->loadJs('vendor/html5-qrcode/html5-qrcode.min.js');
                $this->loadJs('vendor/html5-qrcode/jsqrcode-combined.min.js');
                $this->loadJs('app/test.js');

                
                $this->app->view->render($response,'test/index.twig', $this->data);

	}
} 