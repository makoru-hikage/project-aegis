<?php


/**
 * BaseController
 */

abstract class BaseController {


	/**
	 * searchFilter
	 * @var array
	 */

	protected $searchFilter;


	/**
	 * inputData
	 * @var array
	 */

	protected $inputData;


	/**
	 * execute
	 * @return \Psr\Http\Message\ResponseInterface
	 */
	public function execute() {

	}

}