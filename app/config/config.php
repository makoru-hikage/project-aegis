<?php

$config = [
	'settings' =>
		[
			'displayErrorDetails' => true,
			'determineRouteBeforeAppMiddleware' => true,

			'logger' => [
	    	    'name' => 'slim-app',
	   	        'level' => Monolog\Logger::DEBUG,
	    	    'path' => __DIR__ . '/../app/logs/app.log'
	       	]
	    ]
];