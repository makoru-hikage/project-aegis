<?php
namespace DeltaX\Forms;

use DeltaX\Models\User;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface };
use Symfony\Component\Form\Extension\Core\Type\
                    {TextType, EmailType, ChoiceType, RepeatedType, PasswordType, ButtonType};
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserForm extends AbstractType {

	public function buildForm(FormBuilderInterface $builder, array $options){

        $builder
            ->add('username', TextType::class, [ 'label' => 'Username'])
            ->add('email', EmailType::class, [ 'label' => 'E-mail'])
            ->add('last_name', TextType::class, [ 'label' => 'Last Name'])
            ->add('first_name', TextType::class, [ 'label' => 'First Name'])
            ->add('middle_name', TextType::class, [ 'label' => 'Middle'])
            ->add('sex', ChoiceType::class, [
                'label' => 'Sex',
                'choices'  => [
                    '--' => null,
                    'F' => 'F',
                    'M' => 'M',
                ]])
            ->add('bloodtype', ChoiceType::class, [
                'label' => 'Blood Type',
                'choices'  => [
                    '--' => null,
                    'A+' =>'A+',
                    'B+' =>'B+',
                    'O+' =>'O+',
                    'AB+' =>'AB+',
                    'A-' =>'A-',
                    'B-' =>'B-',
                    'O-' =>'O-',
                    'AB-' =>'AB-'   
                ]])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options'  => ['label' => 'Password'],
                'second_options' => ['label' => 'Repeat Password'],
            ])
            ->add('save', ButtonType::class, array(
                'attr' => array('class' => 'save'),
            ));
    }

    public function configureOptions(OptionsResolver $resolver){
    	
        $resolver->setDefaults(array(
            'data_class' => 'DeltaX\Models\User',
            'action' => '',
            'method'=> 'POST'
        ));
    }
}