<?php
namespace DeltaX\Forms;

use Symfony\Component\Form\{AbstractType, FormBuilderInterface };
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CoursesForm extends AbstractType {

	public function buildForm(FormBuilderInterface $builder, array $options){

        $builder
            ->add('code', TextType::class, ['label' => 'Course Code'])
            ->add('name', TextType::class, ['label' => 'Course Name']);
    }

    public function configureOptions(OptionsResolver $resolver){
    	
        $resolver->setDefaults(array(
            'data_class' => 'DeltaX\Models\Course',
            'action' => '',
            'method'=> 'POST'
        ));
    }
}