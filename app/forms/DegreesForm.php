<?php
namespace DeltaX\Forms;

use Symfony\Component\Form\{AbstractType, FormBuilderInterface };
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DegreesForm extends AbstractType {

	public function buildForm(FormBuilderInterface $builder, array $options){

        $builder
            ->add('id', HiddenType::class)
            ->add('code', TextType::class, 
                [ 
                    'label' => 'Degree Code',
                    'required' => true,  
                    ])
            ->add('name', TextType::class, 
                [ 
                    'label' => 'Degree Name',
                    'required' => true,
                    ])
            ->add('major', TextType::class, 
                [ 
                    'label' => 'Degree Major',
                    'required' => false
                    ]);
    }

    public function configureOptions(OptionsResolver $resolver){
    	
        $resolver->setDefaults(array(
            'data_class' => '\DeltaX\Models\Degree',
            'action' => '/records/degrees',
            'method'=> 'POST',
            'attr' => [
                'class' => 'form-horizontal',
                'id' => 'editor'
                ]
        ));
    }
}