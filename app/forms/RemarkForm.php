<?php
namespace DeltaX\Forms;

use Symfony\Component\Form\{AbstractType, FormBuilderInterface };
use Symfony\Component\Form\Extension\Core\Type\{TextType, TextareaType};
use Symfony\Component\OptionsResolver\OptionsResolver;

class RemarkForm extends AbstractType {

	public function buildForm(FormBuilderInterface $builder, array $options){

        $builder
            ->add('remark', TextType::class, [ 'label' => 'Remark'])
            ->add('description', TextareaType::class, [ 'label' => 'Description']);
    }

    public function configureOptions(OptionsResolver $resolver){
    	
        $resolver->setDefaults(array(
            'data_class' => 'DeltaX\Models\Course',
            'action' => '',
            'method'=> 'POST'
        ));
    }
}