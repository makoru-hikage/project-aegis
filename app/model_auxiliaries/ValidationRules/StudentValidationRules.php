<?php

namespace DeltaX\ValidationRules;

use DeltaX\Crud\ModelService\ValidationRules;
use DeltaX\ContentValidation\CrudValidators as cv;
use Respect\Validation\Validator as v;

class StudentValidationRules extends ValidationRules {

	protected function loadRules(){
		$this->validationRules = [
        	'student_number' => cv::isValidCodename()->length(1, 15),
        	'date_of_enrollment' => v::date(),
        	'last_school_attended' => v::length(null, 255),
        	'year_level' => v::intVal(),
        	'section' => v::length(null, 255),
        	'status' => v::in(['Regular', 'Irregular', 'Shifted', 'Transferred', 'Dropped', 'Expelled']),
        	'degree_id' => cv::isValidCodename()->length(1, 25)
        ];
	}

}