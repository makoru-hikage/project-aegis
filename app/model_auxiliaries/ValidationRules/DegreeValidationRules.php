<?php

namespace DeltaX\Aegis\ValidationRules;

use DeltaX\Crud\ModelService\ValidationRules;
use DeltaX\ContentValidation\CrudValidators as cv;
use Respect\Validation\Validator as v;

class DegreeValidationRules extends ValidationRules {

	protected function loadRules(){
		$this->validationRules = [
			'code' => cv::isValidCodename()->length(null, 25), 
			'name' => v::length(1, 255), 
			'major' => v::length(null, 255) 
		];
	}

}