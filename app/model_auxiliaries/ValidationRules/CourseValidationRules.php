<?php

namespace DeltaX\ValidationRules;

use DeltaX\Crud\ModelService\ValidationRules;
use DeltaX\ContentValidation\CrudValidators as cv;
use Respect\Validation\Validator as v;

class CourseValidationRules extends ValidationRules{

	protected function loadRules(){
		$this->validationRules = [
			'code' => cv::isValidCodename()->length(null, 25), 
			'name' => v::length(1,255), 
			'credits_in_lecture' => cv::isValidCredit(), 
			'credits_in_lab' => cv::isValidCredit()
		];
	}

}