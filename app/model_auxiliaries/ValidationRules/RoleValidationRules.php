<?php

namespace DeltaX\ValidationRules;

use DeltaX\Crud\ModelService\ValidationRules;
use Respect\Validation\Validator as v;

class RoleValidationRules extends ValidationRules {

	protected function loadRules(){
		$this->validationRules = [
			'name' => v::length(1,35),
			'slug' => v::regex('/^[a-zA-Z]/')->alnum('-')->length(1,35),
			'permission' => v::length(1,35)->regex('/^[a-zA-Z]/'),
			'allowed' => v::in([1, 0, true, false])
		];
	}

}