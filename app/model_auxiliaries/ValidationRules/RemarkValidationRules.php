<?php

namespace DeltaX\ValidationRules;

use DeltaX\Crud\ModelService\ValidationRules;
use DeltaX\ContentValidation\CrudValidators as cv;
use Respect\Validation\Validator as v;

class RemarkValidationRules extends ValidationRules {

	protected function loadRules(){
		$this->validationRules = [
			'code' => cv::isValidCodename()->length(1, 25),
			'description' => v::length(null, 255)
		];
	}

}