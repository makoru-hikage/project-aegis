<?php

namespace DeltaX\ValidationRules;

use DeltaX\Crud\ModelService\ValidationRules;
use DeltaX\ContentValidation\CrudValidators as cv;
use Respect\Validation\Validator as v;

class UserValidationRules extends ValidationRules {

	protected function loadRules(){
		$this->validationRules = [
            'username' => cv::isValidUsername(),
            'first_name' => v::length(1,35),
            'middle_name' => v::length(0,35), //some other people have no middle name
            'last_name' => v::length(1,35),
            'email' => v::email(),
            'sex'=> v::in(['F', 'M', '_']),
            'bloodtype'=> v::in(['A+', 'B+', 'O+', 'AB+', 'A-', 'B-', 'O-', 'AB-', 'Other'])
        ];
	}

}