<?php

namespace DeltaX\Aegis\ModelColumns;
use DeltaX\Crud\ModelService\ModelColumns;

class BlankChecklistColumns extends ModelColumns {
	
	protected $htmlColumns = [
		'courses.code' => 'Code',
		'courses.name' => 'Name',
		'sup_credits' => 'Credits',
		'sub_courses.credits_in_lecture' => 'Lec',
		'sub_courses.credits_in_lab' => 'Lab',
		'sub_courses.credits_total' => 'Total',
		'sup_contact_hrs' => 'Contact Hours',
		'sub_lec_contact_hrs' => 'Lec',
		'sub_lab_contact_hrs' => 'Lab',
		'prerequisites' => 'Prerequisites'
	];

	protected $sqlColumns = [
		'courses.code',
		'courses.name',
		'courses.credits_in_lecture',
		'courses.credits_in_lab',
		'curricula.lec_contact_hrs',
		'curricula.lab_contact_hrs',
		'curricula.is_nth_yr_standing',
		'curriculum_terms.year_level'
	];


}