<?php

namespace DeltaX\Aegis\ModelColumns;
use DeltaX\Crud\ModelService\ModelColumns;

class DegreeColumns extends ModelColumns {
	protected $htmlColumns = [
		'code' => 'Code',
		'name' => 'Name',
		'major' => 'Major'
	];

	protected $sqlColumns = [
		'code',
		'name',
		'major'
	];
}