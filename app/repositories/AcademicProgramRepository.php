<?php

namespace DeltaX\Repositories;
use \Prjkt\Component\Repofuck\Repofuck;

class AcademicProgramRepository extends Repofuck {

	protected $resources = [
		'DeltaX\Models\Degree', 
		'DeltaX\Models\Course', 
		'DeltaX\Models\Curriculum', 
		'DeltaX\Models\PrerequisiteCourse', 
		'DeltaX\Models\PrerequisiteSet'
	];
	
}