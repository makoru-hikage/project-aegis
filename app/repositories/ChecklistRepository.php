<?php

namespace DeltaX\Repositories;
use \Prjkt\Component\Repofuck\Repofuck;

class ChecklistRepository extends Repofuck {

	protected $resources = [
		'DeltaX\Models\Remark',
		'DeltaX\Models\Enrollment',
		'DeltaX\Models\Curriculum',
		'DeltaX\Models\Course',
		'DeltaX\Models\Student',
		'DeltaX\Models\CourseSession'
	];
	
}