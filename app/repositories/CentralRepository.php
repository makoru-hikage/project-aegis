<?php

namespace DeltaX\Repositories;

use DeltaX\Crud\ModelService\ModelRepository;

class CentralRepository extends ModelRepository{

	protected $resources = [
            'DeltaX\Models\Degree', //Entity
            'DeltaX\Models\Employee', //Related to a User
            'DeltaX\Models\EnrolledCourse', //Related with Remark, CourseSession, Curriculum, Student, SchoolCalendar
            'DeltaX\Models\Course', //Entity
            'DeltaX\Models\Curriculum', //Related with a Course
            'DeltaX\Models\CurriculumTerm', //Related with a Degree
            'DeltaX\Models\Remark', //Entity
            'DeltaX\Models\SchoolCalendar', //Entity
            'DeltaX\Models\Student', //Entity
            'DeltaX\Models\CourseSession', //Related with a Course, Employee, and SchoolCalendar
            'DeltaX\Models\User', //Entity
            'DeltaX\Models\Role', //Entity
            'DeltaX\Models\Student' //Related to a User
	];

}