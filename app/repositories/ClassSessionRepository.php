<?php

namespace DeltaX\Repositories;
use \Prjkt\Component\Repofuck\Repofuck;

class ClassSessionRepository extends Repofuck {

	protected $resources = [
		'DeltaX\Models\SchoolCalendar', 
		'DeltaX\Models\CourseSession'
	];
}