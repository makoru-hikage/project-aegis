<?php 

$app->get('/', function ($req, $res, $args){

	if ($user){
		$this->get('', 'DashboardController:index');
	} else {
		return $res->withRedirect('login');
	}

});

$app->get('/dashboard', 'DeltaX\Crud\Controllers\RepositoryController:index')->setName('dashboard');
$app->get('/login', 'DeltaX\UserManagement\Controllers\UserController:renderLoginPage')->setName('login');
$app->post('/login', 'DeltaX\UserManagement\Controllers\UserController:userAuthentication')->setName('authentication');
$app->get('/logout', 'DeltaX\UserManagement\Controllers\UserController:logout')->setName('logout');


$app->get('/table/{entity}[/{attrib}/{value}]', 'DeltaX\TableCreation\DataTableGenerator:renderTableBody');
$app->post('/table/{entity}[/{attrib}/{value}]', 'DeltaX\Crud\Controllers\RepositoryController:updateInRepo');

$app->get('/profile[/{id}]', 'DeltaX\Controllers\ProfileController:index');


$app->get('/curriculum/{degree_code}', 'DeltaX\Aegis\Controllers\ChecklistController:getCurriculum');
$app->get('/checklist/{student_number}', 'DeltaX\Aegis\Controllers\ChecklistController:getChecklist');


