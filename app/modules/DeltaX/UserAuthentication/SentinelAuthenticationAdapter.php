<?php


namespace DeltaX\UserAuthentication;

use DeltaX\UserAuthentication\UserAuthenticationAdapterInterface;
use Cartalyst\Sentinel\Sentinel;
use BadMethodCallException;

class SentinelAuthenticationAdapter implements UserAuthenticationAdapterInterface{

	protected $sentinel;

	public function __construct(Sentinel $sentinel){
		$this->sentinel = $sentinel;
	}

	/**
	 * Credentials usually contain username and password
	 * or email.
	 *
	 * @param  array  $credentials 
	 * @param  boolean $remember    
	 * @param  boolean $login    
	 * @return \Cartalyst\Sentinel\Users\UserInterface|bool
	 */
	public function authenticate($credentials, $remember = false, $login = true){		
		return $this->sentinel->authenticate($credentials, $remember, $login);
	}

	/**
	 * Works like authenticate() but no "remember credentials" option.
	 * 
	 * @param  boolean $forced
	 * @return \Cartalyst\Sentinel\Users\UserInterface|bool         
	 */
	public function check(bool $forced = false){
		return $forced ? $this->sentinel->forceCheck() : $this->sentinel->check();
	}

	/**
	 * Get a user object
	 * 
	 * @param  boolean $checked 
	 * @return \Cartalyst\Sentinel\Users\UserInterface|bool         
	 */
	public function getUser(bool $checked = true){
		return $this->sentinel->getUser($checked); 
	}
	
	/**
	 * Register an account
	 * 
	 * @param  array  $credentials
     * @param  \Closure|bool  $callback
     * @return \Cartalyst\Sentinel\Users\UserInteface|bool                
	 */
	public function register(array $credentials, $callback){
		return $this->sentinel->register($credentials, $callback = true);
	}

	/**
	 * Log in an account
	 * 
	 * @param  \Cartalyst\Sentinel\Users\UserInteface  $user
	 * @param  boolean $remember
	 * @return \Cartalyst\Sentinel\Users\UserInteface|bool
	 */
	public function login($user, bool $remember = false){
		return $this->sentinel->login($user, $remember);
	}

	/**
	 * Logs out current user. 
	 * Setting everywhere to true shall flush 
	 * out all user persistence
	 * 
	 * 
	 * @return bool
	 */
	public function logout($user = null, bool $everywhere = false){
		return $this->sentinel->logout($user, $everywhere);
	}

	/**
	 * Hash the plaintext. 
	 * 
	 * @param  string $text 
	 * @return string       
	 */
	public function hashPlaintext(string $plaintext){
		$hasher = $this->sentinel->getHasher();
		return $hasher->hash($plaintext);
	}

	/**
	 * Authorize based on permissions
	 * 
	 * @param  array  $permissions 
	 * @return bool              
	 */
	public function authorize(array $permissions){
		try {
			return $this->sentinel->hasAnyAccess($permissions);
		} catch (BadMethodCallException $e) {
			return false;
		}	
	}
}