<?php

namespace DeltaX\UserAuthentication;

interface UserAuthenticationAdapterInterface {

	/**
	 * Credentials usually contain username and password
	 * or email.
	 *
	 * @param  array  $credentials 
	 * @param  boolean $remember    
	 * @param  boolean $login    
	 * @return object|bool
	 */
	public function authenticate($credentials, $remember, $login);

	/**
	 * Works like authenticate() but no "remember credentials" option.
	 * 
	 * @param  bool $forced
	 * @return object|bool        
	 */
	public function check(bool $forced);

	/**
	 * Get a user object, should it be checked or not?
	 * 
	 * @param  bool $checked 
	 * @return object|bool         
	 */
	public function getUser(bool $checked);

	/**
	 * Register a user account. 
	 * 
	 * @param  array  $credentials
     * @param  \Closure|bool  $callback
     * @return object|bool
	 */
	public function register(array $credentials, $callback);

	/**
	 * Login a user
	 * 
	 * @param  array|object|bool $user     
	 * @param  bool $remember 
	 * @return object|bool
	 */
	public function login($user, bool $remember);

	/**
	 * Logs out a user. Must return true if 
	 * the user is really logged out
	 * 
	 * @param  object|bool $user       
	 * @param  bool $everywhere 
	 * @return bool             
	 */
	public function logout($user, bool $everywhere);

	/**
	 * Use the hash that is usually used by the 
	 * Adapter when it hashes a password.
	 * 
	 * @param  string $plaintext
	 * @return string
	 */
	public function hashPlaintext(string $plaintext);
}