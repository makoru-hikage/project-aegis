<?php

namespace DeltaX\ApiController;

use Interop\Container\ContainerInterface;

class ApiController {

	protected $app;
	
	public function __construct( ContainerInterface $app ){

		$this->app = $app;
		
	}

}