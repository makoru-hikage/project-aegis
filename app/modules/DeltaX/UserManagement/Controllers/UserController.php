<?php

namespace DeltaX\UserManagement\Controllers;

use Interop\Container\ContainerInterface;
use DeltaX\FrontEndController\FrontEndController;

class UserController extends FrontEndController
{

    public function index($request, $response, $args){

        $this->app->view->render($response,'profile/index.twig', $this->data);      
    }

    public function userAuthentication($request, $response, $args){

        $credentials = $request->getParsedBody();
        $credentials = [
            'login' => $credentials['username'],
            'password' => $credentials['password']
        ];

        $authenticated = $this->app->auth->authenticate($credentials);
        
        $message = $authenticated ? "Successfully logged in!" : "Invalid username or password";
        $code = $authenticated ? 200 : 401;

        return $response->withJson(['message'=>$message], $code);

    }

    public function renderLoginPage ($request, $response, $args){
        $this->data['title'] = 'Login';
        $this->app->view->render($response,'user/login.twig', $this->data);  
    }

    public function logout($request, $response, $args){
        $this->app->auth->logout();

        return $response->withJson([ 'message' => "Bye! Thank you. Logged out." ], 200);
    }

    

}