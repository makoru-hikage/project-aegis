<?php

namespace DeltaX\Utilities;

/**
 * Renames a key in array
 * 
 * @param array $haystack
 * @param string $needle
 * @param string $replacement
 * @return array
 */
function array_rename_key($haystack, $needle, $replacement){
	$keys = array_keys($haystack);
    $index = array_search($needle, $keys);

    if ($index !== false) {
        $keys[$index] = $replacement;
        $haystack = array_combine($keys, $haystack);
    }

    return $haystack;
}

/**
 * Tests if an array is an associative one 
 * 
 * @param array $arr
 * @return bool
 */
function is_assoc_array(array $arr){
    if (empty($arr)) return false;
    return array_keys($arr) !== range(0, count($arr) - 1);
}

/**
 * Filters an array based on a another array
 * 
 * @param array $input
 * @param array $filter
 * @return array
 */
function array_filter_keys(array $input, array $filter){

    //If the filter is an associative array, take its keys instead
	$filter = is_assoc_array($filter) ? array_keys($filter) : $filter;

	$input = array_filter($input, function($item) use ($filter){
		return in_array($item, $filter);
	}, ARRAY_FILTER_USE_KEY);

	return $input;
}

/**
 * Converts an integer into its ordinal form (1 = 1st)
 *
 * @param int $num
 * @return string 
 */
function ordinal($number) {
    $ends = ['th','st','nd','rd','th','th','th','th','th','th'];
    return ((($number % 100) >= 11) && (($number%100) <= 13)) 
        ? $number. 'th'
        : $number. $ends[$number % 10];
}

/**
 * Turns numeric strings into integer or float
 * 
 * @return string|int|float $num
 */
function array_strings_to_numbers(array $arr){

    $convertToNumber = function ($num){
        return $num + 0;
    };

    return array_map($convertToNumber, $arr);
}
