<?php
namespace DeltaX\TableCreation;

use Donquixote\Cellbrush\Table\Table;
use DeltaX\Utilities as U;



class DataTableGenerator {

    /**
     * Make a string for making an HTML table. 
     * For some reason, the table must be named.
     * This supports only one-tiered and two-tiered table headers.
     *
     * @param string $rowName
     * @param array $headers
     * @return string
     * 
     */
    public static function createTableWithHeader(string $rowName, array $headers){
        
        $table = Table::create();

        $table->thead()->addRowName($rowName);
        $currentSuperCell = null;

        foreach ($headers as $colName => $label) {

            switch ($colName) {
                case preg_match('/^sub_/', $colName) ? true : false :
                    
                    $table->thead()->addRowIfNotExists($rowName.'_sub');
                    $editedColName = preg_replace('/^sub_/', $currentSuperCell.'.', $colName);
                    $headers = U\array_rename_key($headers, $colName, $editedColName);
                    $table->thead()->th($rowName.'_sub', $editedColName, $label);
                    break;

                case preg_match('/^sup_/', $colName) ? true : false :

                    $currentSuperCell = preg_replace('/^sup_/', '', $colName);
                    $headers = U\array_rename_key($headers, $colName, $currentSuperCell);
                    $table->thead()->th($rowName, $currentSuperCell, $label);
                    break;
                    
                default:
                    $table->thead()->th('', $colName, $label);
                    break;
            }
        }

        $table->addColNames(array_keys($headers));

        return $table;
    }
}