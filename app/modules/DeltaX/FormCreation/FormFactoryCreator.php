<?php

namespace DeltaX\FormCreation;

use \Symfony\Component\Form\Forms;
use \Symfony\Component\Form\Extension\HttpFoundation\HttpFoundationExtension;
use \Symfony\Component\Form\Extension\Csrf\CsrfExtension;

use \Symfony\Component\Security\Csrf\TokenStorage\NativeSessionTokenStorage;
use \Symfony\Component\Security\Csrf\TokenGenerator\UriSafeTokenGenerator;
use \Symfony\Component\Security\Csrf\CsrfTokenManager;

class FormFactoryCreator {

	public static function createCsrfProtection(){

		$csrfGenerator = new UriSafeTokenGenerator();
    	$csrfStorage = new NativeSessionTokenStorage('\Symfony\Component\HttpFoundation\Session\Session');
    	$csrfManager = new CsrfTokenManager($csrfGenerator, $csrfStorage);

    	return new CsrfExtension($csrfManager);
	}

	public static function createFormFactory(){
		return Forms::createFormFactoryBuilder()
	        ->addExtension(new HttpFoundationExtension())
	        ->addExtension(self::createCsrfProtection())
	        ->getFormFactory();
	}
}