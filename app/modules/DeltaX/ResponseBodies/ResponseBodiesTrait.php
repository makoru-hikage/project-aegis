<?php

namespace DeltaX\ResponseBodies;

trait ResponseBodiesTrait {
	
	public static function createResponseBody(bool $success, $data, $message, int $code){
		return [
			'success' => $success, 
			'data' => $data, 
			'message' => $message, 
			'code' => $code	
		];
	}

	public static function createSuccessfulCrudResponse($data, $message = "Alright!"){
		return self::createResponseBody(true, $data, $message, 200);
	}

	public static function createUnauthorizedAccessResponse($message = "Unauthorized"){
		return self::createResponseBody(false, null, $message, 401);
	}

	public static function createNotFoundResponse($message = "Content Not Found"){
		return self::createResponseBody(false, null, $message, 404);
	}

	public static function createNotAllowedResponse($message = "Method Not Allowed"){
		return self::createResponseBody(false, null, $message, 405);
	}

	public static function createServerErrorResponse($message = "Server Error"){
		return self::createResponseBody(false, null, $message, 500);
	}

	public static function createUnprocessableEntityResponse($data, $message = "Unprocessable Entity"){
		return self::createResponseBody(false, $data, $message, 422);
	}

	public static function createBadRequestResponse($data, $message = "Bad Request"){
		return self::createResponseBody(false, $data, $message, 400);
	}

	public static function checkForFailure($condition, callable $failure, $data, $message = null){
		$responseBody = array();
		if ($condition){
			$responseBody = self::createSuccessfulCrudResponse($data);
		} else {
			$responseBody = call_user_func($failure, $data, $message);
		}

		return $responseBody;
	}

	public static function checkSqlError(\Illuminate\Database\QueryException $e){
		$code = 0;
		$message = 'Server Error';

		switch ($e->errorInfo[1]) {
			 	case 1062:
			 		$code = 422;
					$message = $e->errorInfo[2];
			 		break;
			 		
			 	case 1054:
			 		$code = 422;
					$message = $e->errorInfo[2];
			 		break;

			 	case 1406:
			 		$code = 400;
					$message = $e->errorInfo[2];
			 		break;
			 	
			 	default:
			 		$code = 500;
					$message = 'Server Error';
			 		break;
		}

		return self::createResponseBody(false, null, $message, $code);
	}

}