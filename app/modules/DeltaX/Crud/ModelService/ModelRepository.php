<?php

namespace DeltaX\Crud\ModelService;

use Prjkt\Component\Repofuck\Repofuck;
use Prjkt\Component\Repofuck\Exceptions\InvalidCallbackReturn;
use Illuminate\Database\Eloquent\{ Model, Builder };
use Closure;

/**
 * Description of ModelRepository
 *
 * @author DeltaX
 */
abstract class ModelRepository extends Repofuck{

	public function modifyEntity(Closure $closure){
		
		if ( $closure instanceof Closure ) {
			
			$return = call_user_func_array($closure, [$this->entity]);

			if ( ! $return instanceof Model && ! $return instanceof Builder ) {
				throw new InvalidCallbackReturn;

			}

			$this->entity = $return;

		}

		return $this;
	}

	public function turnToPage(int $items = null, $columns = null, $page = 1){

		$items = is_null($items) ? $this->paginates : $items;
		$columns = is_null($columns) ? $this->columns : $columns;
		
		return $this->entity->paginate($items, $columns, "page", $page);
	}

	public function toSql() {

		return $this->entity->toSql();

	}
}
