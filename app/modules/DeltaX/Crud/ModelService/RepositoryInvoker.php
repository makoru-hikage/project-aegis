<?php

namespace DeltaX\Crud\ModelService;

use Illuminate\Database\Eloquent\Model;
use DeltaX\Crud\ModelService\ModelRepository;
use Prjkt\Component\Repofuck\Exceptions\EntityNotDefined;

class RepositoryInvoker {

	protected $repo;

	public function __construct(ModelRepository $repo){
		$this->repo = $repo;
	}

	public function entity($entityName, $closure = null){

		try{
			$this->repo->entity($entityName, $closure);			
		} catch (EntityNotDefined $e){
			return null;
		}

		return $this;
	}

	public function where($clause, $append = false){

		$this->repo->where($clause, $append);
		
		return $this;
	}

	public function getRows($entityName, $columns = '*', $whereParams = null){
		
		if(!$this->entity($entityName)){
			return null;
		}

		if($whereParams){
			$this->where($whereParams);
		}

		$this->where(['is_deleted', 0], true);
		
		return $this->repo->get($columns);

	}

	public function get($columns = null){

		return $this->repo->get($columns);
	}

	public function toSql(){

		return $this->repo->toSql();

	}

	public function find($entityName, $params, $value, $columns = null) {
		
		$repo = $this->repo;

		if(!$this->entity($entityName)){
			return null;
		}

		if ($columns) {
			$repo = $repo->setColumns($columns);
		}
		
		return $repo->first($params, $value);
	}

	public function create($entityName, $params) : Model {
		
		return $this->$repo
			->entity($entityName)
			->data($params)
			->create();

	}

	public function update($entityName, $idValue, $params) : Model {

		return $this->repo
			->entity($entityName)
			->data($params)
			->update($idValue);

	}

	public function updateOrCreate($entityName, $id, $params){

		$entity = $this->find($entityName, $id);

		return empty($entity) || empty($id) ? 
			$this->create($entityName, $params) :
			$this->update($entityName, $id, $params);

	}

	public function delete($entityName, $idValue) : Model {
		return $this->update($entityName, $idValue, ['is_deleted' => 1]);
	}

	public function undelete($entityName, $idValue) : Model {
		return $this->update($entityName, $idValue, ['is_deleted' => 0]);
	}

	public function modifyEntity($closure){
		$this->repo->modifyEntity($closure);
		return $this;
	}

	public function paginate(int $items = null, $columns = null, $page = 1){
		return $this->repo->turnToPage($items, $columns, $page);
	}

	public function orderBy(array $columns){

		$closure = function ($entity) use ($columns){
			foreach ($columns as $column => $value) {
				$entity = $entity->orderBy($column, $value);
			}

			return $entity;
		};

		return $this->modifyEntity($closure);
	}

}