<?php

namespace DeltaX\Crud\ModelService;

abstract class ModelColumns {

	/**
	 * An associative array where the keys are for SQL and
	 * the values are for HTML Tables. 
	 * 
	 * Supports only two-tiered table headers. In case the 
	 * headers are two-tiered, add "sup_" as a prefix of 
	 * the encompassing cell; subsequent cells, which must
	 * be prefixed with "sub_", are encompassed by the 
	 * immediate preceding "sup_" cell
	 *
	 * @var array
	 */
	protected $htmlColumns = [];

	/**
	 * Something to supply in SQL SELECT statement
	 *
	 * @var array
	 */
	protected $sqlColumns = [];
	
	/**
	 * Get all of the columns
	 *
	 * @return array|string
	 */
	public function getHtmlColumns(){
		return $this->htmlColumns;	
	}

	/**
	 * Prepare columns for an SQL query
	 *
	 * @return array
	 */
	public function getSqlColumns(){
		return $this->sqlColumns;
    }

}