<?php

/*
 * Delta X
 * 
 */

namespace DeltaX\Crud\ModelService;

use DeltaX\Crud\ModelService\ModelRepository;
use DeltaX\Crud\ModelService\RepositoryInvoker;

/**
 * Description of ModelService
 *
 * @author DeltaX
 */
class ModelService {
	
	protected $repoInvoker;

	protected $outputData;

	public function __construct(ModelRepository $repository) {
		$this->repoInvoker = new RepositoryInvoker($repository);
	}

	public function getOutputData(){
		return $this->outputData;
	}

}
