<?php

namespace DeltaX\Crud\ModelService;

use DeltaX\Crud\SearchQuery\SearchFilter;
use DeltaX\Crud\SearchQuery\SearchFilterItem;
use DeltaX\Exceptions\InvalidSearchFilterException;
use DeltaX\Exceptions\NullItemException;
use DeltaX\Crud\ModelService\ModelService;
use Illuminate\Pagination\LengthAwarePaginator;

abstract class CollectionModelService extends ModelService {

	/**
	 * A key value pair of columns and the
	 * name of the table where they belong,
	 * Good for disambigating columns in 
	 * WHERE clause
	 * 
	 * @var array|DeltaX\Crud\SearchQuery\SearchFilter;
	 */
	
	protected $searchFilter;

	/**
	 * The name of the main table 
	 * where data is extracted
	 * 
	 * @var string 
	 */
	protected $mainTable;

	/**
	 * Items per page
	 * 
	 * @var int
	 */
	
	protected $per_page = 15;

	/**
	 * The current page
	 * 
	 * @var int
	 */
	
	protected $page = 0;

	/**
	 * An array of columns to determine
	 * how the items must be sorted.
	 * Should it be prefixed, it must 
	 * be '-' or '+'. No prefix means
	 * it is in ascending order
	 * 
	 * @var [type]
	 */
	
	protected $sort;

	/**
	 * To be used by prepareSearchFilter()
	 * method
	 * 
	 * @var array
	 */
	protected $paramsWithFullName;

	/**
	 * To be used by the customClause()
	 * method or fetchDataFromForeignTables()
	 * 
	 * @var array
	 */
	protected $specialParams;

	/**
	 * Rename the columns of the search filter to
	 * resolve any ambiguities in the column names
	 * Make sure to supply necessary array for 
	 * $paramsWithFullName property.
	 * 
	 * @return self
	 */
	protected function renameColumns(){

		$params = $this->paramsWithFullName;
		$searchFilter = $this->searchFilter;

		foreach ($params as $name => $column) {
			try {
				$searchFilter->renameColumn($name, $column);
			} catch (NullItemException $e) {
				continue;
			}
		}

		if ($searchFilter->getFilter('sort')) {
			$this->searchFilter = $this->renameSortFields($searchFilter, $params);
		}
		return $this;
	}

	/**
	 * Should there be a 'sort' amongst the search filter
	 * It'll be the same with renameColumns method, but
	 * deals with prefixes such as '+' and '-'
	 * 
	 * @param  SearchFilter $searchFilter     
	 * @param  array        $oldAndNewNamePair
	 * @return SearchFilter 
	 */
	protected function renameSortFields(SearchFilter $searchFilter, array $oldAndNewNamePair){

		if ($searchFilter->getFilter('sort')) {

			$sortFields = $searchFilter->getFilter('sort')->getValue();

			foreach ($sortFields as $index => $sortField) {

				$firstChar = substr($sortField, 0, 1);
				$prefix = "";

				switch ($firstChar) {
					case '+':
					case '-':
						$prefix = $firstChar;
						$sortField = substr($sortField, 1);
						break;
					case ctype_alpha($firstChar):
						break;
				}				

				if (! array_key_exists($sortField, $oldAndNewNamePair)) {
					continue;
				}

				$sortFields[$index] = $prefix . $oldAndNewNamePair[$sortField];	
			}

			$searchFilter = $searchFilter->editFilter('sort', '=', $sortFields);
		}
		return $searchFilter;
	}

	/**
	 * Sets the main table for query building.
	 * The child's $mainTable must be defined
	 * 
	 * @return self
	 */
	protected function prepareMainTable(){

		$this->repoInvoker->entity($this->mainTable);

		return $this;
	}

	/**
	 * Run the query 
	 * 
	 * @return Illuminate\Database\Eloquent\Collection|null
	 */
	protected function runQuery(){

		$result = $this->page > 0 ?
			$this->repoInvoker->paginate($this->per_page, null, $this->page) : 
			$this->repoInvoker->get();

		if ($result instanceof LengthAwarePaginator) {
			$collection = $result->getCollection();
			$collection = $this->modifyOutputData($collection);
			return $result->setCollection($collection);
		}

		$result = $this->modifyOutputData($result);
		return $result;
	}

	/**
	 * Sets the Search filter for the WHERE clause
	 * and other clauses.
	 * 
	 * @param \DeltaX\Crud\SearchQuery\SearchFilter $searchFilter 
	 * @return self 
	 */
	public function setSearchFilter($searchFilter){

		$this->searchFilter = $searchFilter;
		
		return $this->renameColumns();
	}

	/**
	 * Add an item in the SearchFilter
	 * 
	 * @param DeltaX\Crud\SearchQuery\SearchFilterItem|array $item 
	 * @return self
	 */
	public function addFilterItem($item){

		switch ($this->searchFilter) {
			case $this->searchFilter instanceof SearchFilter:
				$this->searchFilter = $this->searchFilter->addFilter($item);
				break;

			case is_array($this->searchFilter);
				$this->searchFilter[] = $item;
				break;

			default:
				$message = "Insert only a SearchFilterItem or anything to make SearchFilterItem";
				throw new InvalidSearchFilterException ($message);
				break;
		}

		return $this;
	}

	/**
	 * The array input should be like this:
	 * ['column1' => 'asc', 'column2' => 'desc']
	 * 
	 * @return self
	 */
	protected function prepareSortBy(){

		if (isset($this->sort)) {
			$sortBy = $this->sort;

			$this->repoInvoker->orderBy($sortBy);
		}

		return $this;
	}

	/**
	 * Prepare a WHERE BETWEEN clause
	 * 
	 * @param  string $column 
	 * @param  array  $range  
	 * @return self         
	 */
	protected function whereBetween(string $column, array $range){

		$this->repoInvoker->modifyEntity(function ($entity) use ($column, $range){
			return $entity->whereBetween($column, $range);
		});

		return $this;

	}

	/**
	 * Prepare a WHERE IN clause
	 * 
	 * @param  string $column 
	 * @param  array  $range  
	 * @return self         
	 */
	protected function whereIn(string $column, array $values){

		$this->repoInvoker->modifyEntity(function ($entity) use ($column, $values){
			return $entity->whereIn($column, $values);
		});

		return $this;

	}

	/**
	 * Prepare a WHERE NOT BETWEEN clause
	 * 
	 * @param  string $column 
	 * @param  array  $range  
	 * @return self         
	 */
	protected function whereNotBetween(string $column, array $range){

		$this->repoInvoker->modifyEntity(function ($entity) use ($column, $range){
			return $entity->whereNotBetween($column, $range);
		});

		return $this;

	}

	/**
	 * Prepare a WHERE NOT IN clause
	 * 
	 * @param  string $column 
	 * @param  array  $values  
	 * @return self        
	 */
	protected function whereNotIn(string $column, array $values){

		$this->repoInvoker->modifyEntity(function ($entity) use ($column, $values){
			return $entity->whereNotIn($column, $values);
		});

		return $this;

	}

	/**
	 * Prepare all the WHERE clause parameters
	 * that have comparative operators and LIKE
	 * 
	 * @param  SearchFilterItem|SearchFilter  $filter  
	 * @return self
	 */
	protected function where($filter){

		if ( ! empty($filter) ) {

			$args  = [];

			switch ($filter) {
				case $filter instanceof SearchFilter:
					$args = $filter->toArray();
					break;
				
				case $filter instanceof SearchFilterItem:
					$args = $filter->toArray();
					$args = [ $args ];
					break;

				default:
					throw new InvalidSearchFilterException;
			}

			$this->repoInvoker->where(function ($entity) use ($args){
				return $entity->where($args);
			}, true);
		}

		return $this;
	}

	/**
	 * Use the Search Filter to set the WHERE clauses
	 * on the Builder
	 * 
	 * @return self
	 */
	protected function prepareSearchFilter(){

		if (is_null($this->searchFilter)){
			return $this;
		}


		$items = $this->searchFilter->getFilter();
		foreach ($items as $key => $item) {

			$operator = $item->getOperator();
			$column = $item->getColumn();
			$value = $item->getValue();

			if (in_array($column, ['sort', 'page', 'per_page'])) {
				$this->{$column} = $column === 'sort' ? 
					$this->setSort($value) : 
					$value;
				continue;
			}

			if ( ! $this->isParamNameAllowed($key) ) {
				continue;
			}

			switch ($operator) {
				case 'between':
					$this->whereBetween($column, $value);
					break;

				case 'nbetween':
					$this->whereNotBetween($column, $value);
					break;

				case 'in':
					$this->whereIn($column, $value);
					break;

				case 'nin':
					$this->whereNotIn($column, $value);
					break;
				
				default:
					$this->where($item);
			}
			
		}
		
		return $this;

	}

	/**
	 * The dev must make a method named "customClause"
	 * to modify the Builder by introducing a custom
	 * clause. Such method returns "$this" 
	 * 
	 * @return self
	 */
	protected function runCustomClause(){

		if (method_exists($this, 'customClause')) {
			$this->customClause();
		}

		return $this;
	}

	/**
	 * Items must be like this: col1, -col2, +col3
	 * Anything prefixed with "-" is for descending order
	 */
	protected function setSort(array $items){

		$columnOrders = [];

		foreach ($items as $item) {

			$itemStartsWithPunctuation = preg_match('/^\W[A-Za-z]*/', $item);

			if ($itemStartsWithPunctuation) {

				$keyValuePair = $this->determinePrefixedSortColumn($item);

				if ($keyValuePair === null){
					continue;
				}

				$columnOrders[$keyValuePair[0]] = $keyValuePair[1];

				continue;
			}

			$columnOrders[$item] = 'asc';
		}

		return $columnOrders;
	}

	/**
	 * Prepares an item in $sort array
	 * 
	 * @param  string $item 
	 * @return array|null
	 */
	protected function determinePrefixedSortColumn(string $item){

		$column = substr($item, 1);
				
		switch (substr($item, 0, 1)) {
			case '+':
				return [$column, 'asc'];
				break;

			case '-':
				return [$column, 'desc'];
				break;

			default:
				return null;
		}
	}

	/**
	 * What do you delegate in the SELECTion of columns?
	 * 
	 * @return self
	 */
	abstract protected function prepareColumns();

	
	/**
	 * Make table joins with this method
	 * 
	 * @return self
	 */
	abstract protected function fetchDataFromForeignTables();


	/**
	 * Run the query done by the Repository Invoker
	 * 
	 * @return self
	 */
	public function prepareData(){

		$this->outputData = $this
			->fetchDataFromForeignTables()
			->prepareMainTable()
			->prepareSearchFilter()
			->runCustomClause()
			->prepareColumns()
			->prepareSortBy()
			->runQuery();
				
		return $this;
	}

	/**
	 * Process the data and get the output.
	 * 
	 * @return mixed
	 */
	public function getPreparedOutputData(){

		return $this->prepareData()->getOutputData();

	}

	/**
	 * Override this method should you want to modify 
	 * data after running the query when you extend 
	 * this class
	 * 
	 * @param  mixed $data
	 * @return mixed      
	 */
	protected function modifyOutputData($data){
		return $data;
	}

	protected function isParamNameAllowed(string $param){

		$allowed = true;

		if ( isset($this->specialParams) ) {
			$allowed = in_array($param, $this->specialParams);
		}

		if ( isset($this->paramsWithFullName) ) {
			$allowed = $allowed || array_key_exists($param, $this->paramsWithFullName);
		}

		return $allowed;
	}


} 