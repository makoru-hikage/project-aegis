<?php

namespace DeltaX\Crud\Controllers;

use DeltaX\FrontEndController\FrontEndController;
use Interop\Container\ContainerInterface;
use DeltaX\Crud\ModelService\RepositoryInvoker;
use DeltaX\Repositories\CentralRepository;
use DeltaX\TableCreation\DataTableGenerator;
use DeltaX\FormCreation\FormFactoryCreator;
use Illuminate\Support\Str;

class CrudPageController extends FrontEndController {

        public function __construct(ContainerInterface $c = null){
                parent::__construct($c);
                $this->repoInvoker = new RepositoryInvoker(new CentralRepository);
        }
	
	public function index($request, $response, $arguments){

		$this->data['title'] = 'Data Repository';

                $entityName = $arguments['entity'];
                $formClassName = '\DeltaX\Forms\\'.Str::studly($entityName).'Form';
                $entityName = $arguments['entity'];
                $entityColumnHeads = $this->repoInvoker->getEntityColumnHeads($entityName);
                

                $this->data['table'] = $this->prepareTable($entityColumnHeads);
                $form_factory = FormFactoryCreator::createFormFactory();
                $this->data['form'] = $form_factory->create($formClassName)->createView();
                
                $this->publish('entityName', $entityName);
                $this->loadJs('app/crud_table.js');
                $this->loadJs('app/crud_fields/'.$entityName.'_field.js');
                $this->loadCss('vendor/footable/footable.bootstrap.css');
                $this->loadJs('vendor/footable/footable.min.js');
                $this->loadJs('app/crud_table.js');
                
                $this->app->view->render($response,'crud/index.twig', $this->data);

	}

        public function prepareTable($columnHeads){

                $table = DataTableGenerator::createTableWithHeader('crud_headers', $columnHeads);
                $table->setAttribute('id', 'crud-datatable');
                $table->setAttribute('data-sorting', 'true');
                $table->addClass('table');

                return $table->render();
        }
} 