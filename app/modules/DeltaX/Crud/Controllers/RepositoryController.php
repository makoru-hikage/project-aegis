<?php

namespace DeltaX\Crud\Controllers;

use DeltaX\Repositories\CentralRepository;
use DeltaX\Crud\ModelService\RepositoryInvoker;
use Illuminate\Database\QueryException;


class RepositoryController {

	use \DeltaX\ResponseBodies\ResponseBodiesTrait;

	protected $repoInvoker;

	public function __construct(){
		$this->repoInvoker = new RepositoryInvoker(new CentralRepository);
	}

	public function findInRepo($request, $response, $arguments){

		$entityName = $arguments['entity'];
		$id = $arguments['id'];

		$responseBody = $this->executeRepositoryInvoker('find', $entityName, $id);
		return $response->withJson($responseBody, $responseBody['code']);
	}

	public function rowsFromRepo($request, $response, $arguments){

		if (!isset($arguments['attrib']) || !isset($arguments['value'])){
			$whereParams = null;
		} else {
			$whereParams = [ [ $arguments['attrib'], $arguments['value'] ] ];
		}
		$entityName = $arguments['entity'];

		$responseBody = $this->executeRepositoryInvoker('getRows', $entityName, $whereParams);
		return $response->withJson($responseBody, $responseBody['code']);

	}

	public function storeIntoRepo($request, $response, $arguments){

		$entityName = $arguments['entity'];
		$input = $request->getParsedBody();

		$responseBody = $this->executeRepositoryInvoker('create', $entityName, $input);
		return $response->withJson($responseBody, $responseBody['code']);
	}

	public function updateInRepo($request, $response, $arguments){

		$id = $arguments['id'];
		$entityName = $arguments['entity'];
		$input = $request->getParsedBody();

		$responseBody = $this->executeRepositoryInvoker('update', $entityName, $id, $input);
		return $response->withJson($responseBody, $responseBody['code']);

	}

	public function deleteInRepo($request, $response, $arguments){
		
		$entityName = $arguments['entity'];
		$id = $arguments['id'];

		$responseBody = $this->executeRepositoryInvoker('delete', $entityName, $id);
		return $response->withJson($responseBody, $responseBody['code']);
		
	}

	public function executeRepositoryInvoker($method, ...$args){

		$responseBody = null;
		$entity = null;

		try {
			$entity = call_user_func_array([$this->repoInvoker, $method], $args);
			return $entity ? $this->createSuccessfulCrudResponse($entity) : $this->createNotFoundResponse();
		} catch (QueryException $e){
			$responseBody = self::checkSqlError($e);
		} catch (BadMethodCallException $e){
			return self::createServerErrorResponse("Repository Invoker does not support that function");
		}
		return $responseBody;
	}

}