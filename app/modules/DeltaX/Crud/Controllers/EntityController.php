<?php

namespace DeltaX\FrontEndController;
use Interop\Container\ContainerInterface;

	/**
	* 
	*/
abstract class EntityController{
		
	/**
	 * app
	 * @var \Interop\Container\ContainerInterface
	 */

	protected $app;

	public function __construct(ContainerInterface $ci){
		$this->app = $ci;
	}

	/**
	 * run
	 * @param $request
	 * @param $response
	 * @param $arguments
	 * @return void
	 */
	public function run($request, $response, $arguments) {

	}

}