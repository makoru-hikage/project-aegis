<?php

namespace DeltaX\Crud\SearchQuery;

use DeltaX\Exceptions\InvalidOperatorException;

class SearchFilterItem {

	/**
	 * VALID_OPERATORS
	 * @var array
	 */

	const VALID_OPERATORS = [
        'eq' => '=', 
        'gt' => '<', 
        'lt' => '>', 
        'gte' => '<=', 
        'lte' => '>=', 
        'ne' => '!=',
        'like' => 'like', 
        'nlike' => 'not like', 
        'bwn' => 'between',
        'nbwn' => 'nbetween',
        'in' => 'in',
        'nin' => 'nin'
    ];


	/**
	 * Made for avoiding duplication
	 * of columns in a set called
	 * SearchFilter
	 * @var string
	 */

	protected $name;

	/**
	 * The column
	 * @var string
	 */

	protected $column;


	/**
	 * operator
	 * @var string
	 */

	protected $operator;


	/**
	 * value
	 * @var mixed
	 */

	protected $value;

	/**
	 * __construct
	 *
	 * @param string|array $column
	 * @param string $operator
	 * @param mixed $value
	 * @return self
	 * @throws \DeltaX\Exceptions\InvalidOperatorException 
	 */
	public function __construct($column, string $operator = null, $value = null) {

		if ( is_array($column) && ! is_null($column[0]) ){
			$this->column = $column[0];
			$this->operator = $column[1];
			$this->value = $column[2];

			if ( ! in_array($column[1], self::VALID_OPERATORS) ){
				$this->operator = '=';
				$this->value = $column[1];
			}

		} else {
			$this->column = $column;
			$this->operator = $operator;
			$this->value = $value;
		}

		$this->name = $this->column;

	}

	/**
	 * Returns the name property
	 * 
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Returns the column property
	 * 
	 * @return string
	 */
	public function getColumn() {
		return $this->column;
	}

	/**
	 * Returns the operator property
	 * 
	 * @return string
	 */
	public function getOperator() {
		return $this->operator;
	}

	/**
	 * Returns the value property
	 * 
	 * @return mixed
	 */
	public function getValue() {
		return $this->value;
	}

	/**
	 * Sets the value and operator
	 * 
	 * @param mixed $value
	 * @param string $operator
	 * @return mixed
	 */
	public function setValueAndOperator($value, $operator = '=') {
		$this->value = $value;
		$this->operator = $operator;

		return $this;
	}

	/**
	 * Is the operator vaild?
	 * 
	 * @return bool
	 */
	public function operatorIsValid() {
		return in_array($this->operator, static::VALID_OPERATORS);
	}

	/**
	 * Edit a column
	 * 
	 * @param string $tableName 
	 * @return  self
	 */
	public function renameColumn(string $columnName){

		$this->column = $columnName;

		return $this;
	}

	/**
	 * Returns an indexed array representation
	 * 
	 * @return array
	 */
	public function toArray() {
		return [
			$this->column,
			$this->operator,
			$this->value
		];
	}

	/**
	 * See json_encode in PHP manual
	 * 
	 * @param int $options
	 * @return array
	 */
	public function toJson(int $options = null) {
		return json_encode($this->toArray(), $options);
	}

	/**
	 * Returns a JSON of the object
	 * For further options, call toJson
	 * with arguments instead
	 * 
	 * @return string
	 */
	public function __toString() {
		return $this->toJson();
	}

}
