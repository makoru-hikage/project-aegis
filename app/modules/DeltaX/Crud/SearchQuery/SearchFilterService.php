<?php

namespace DeltaX\Crud\SearchQuery;

use DeltaX\Crud\SearchQuery\SearchFilter;
use DeltaX\Crud\MenuService\MenuService;

class SearchFilterService extends MenuService {

	protected function processData(){

		$searchFilterItems = [];

		foreach ($this->inputData as $key => $queryValue) {
			
			//It is always assumed that the values are lone strings.
			$operator = '=';
			$value = $queryValue;

			//Should a comma be found amidst the said string.
			if (strpos($value, ',') !== false) {
	
				//Turn it into an array 
				//such as that col1 => "val0, val1"
 				$value = explode(',', $value);

				//Should the URL query param looks like 'col1' => ['ne', 'val']
				if (array_key_exists($value[0], SearchFilter::VALID_OPERATORS)) {
					$operator = SearchFilter::VALID_OPERATORS[$value[0]];
					$value = array_slice($value, 1);
				}

				//Items with one-element arrays are turned into their values
				//Such as that col1 => [1] turns to col1 => 1
				$value = count($value) > 1 ? $value : $value[0];

			}

			// All Query Params are treated as string, should the value be a number, 
			// convert it.
			$value = is_numeric($value) ? $value + 0 : $value;

			//"sort" is a special item in a search filter
			$value = $key === 'sort' && ! is_array($value) ? [ $value ] : $value;

			$searchFilterItems[$key] = [$key, $operator, $value];
		}

		$this->outputData = $searchFilterItems;

		return $this;
	}

}