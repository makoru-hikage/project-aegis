<?php

namespace DeltaX\Crud\SearchQuery;

use DeltaX\Crud\SearchQuery\SearchFilter;
use DeltaX\Crud\InputValidation\ValidatorService;

class SearchValidatorService extends ValidatorService {

	/**
	 * Validate the input
	 * 
	 * @return self
	 */
	protected function processData() {

		$keyValuePairOnly = true;

		$originalInput = $this->inputData;

		$this->inputData = (new SearchFilter($this->inputData))->toArray($keyValuePairOnly);

		$this->validate();
		
		if (count($this->outputData) < 1) {
			$this->outputData = $originalInput;
			return $this;
		}
		
		return $this;

	}

}