<?php

namespace DeltaX\Crud\MenuService;

/**
 * MenuService
 */

abstract class MenuService {

	/**
	 * Response: The Status Code
	 * 
	 * @var int
	 */
	protected $code = 0;

	/**
	 * Response: The main content 
	 * of the response body
	 * 
	 * @var mixed
	 */
	protected $outputData;


	/**
	 * The data to be processed
	 * 
	 * @var array
	 */
	protected $inputData = [];

	/**
	 * The constructor
	 * 
	 * @param array $input
	 * @return void 
	 */
	public function __construct(array $input = []){

		if (! empty($input)) {
			$this->inputData = $input;	
		}	
	}


	/**
	 * Accept an array of instances of 
	 * MenuService. Each will process their
	 * input data then return their output 
	 * data so the next instance can use 
	 * the output data as their input data. 
	 * When there is no instance left in 
	 * the array, the last one shall be returned
	 *
	 * @param self ...$menuService
	 * @return self
	 */
	public function prepend(self ...$menuServices){

		$remainingMenuSvcs = $menuServices;
		$currentMenuSvc = array_shift($remainingMenuSvcs);

		//Prepare and "prepend" the remaining MenuServices
		if ( $currentMenuSvc && $this->code === 0 ) {

			$currentMenuSvc = $currentMenuSvc->setInputData($this->inputData);
			$currentMenuSvc = $currentMenuSvc->setCode($this->code);

			$msData = $currentMenuSvc->processData()->prepareResponse();
			
			$this->code = $msData['code'];
			$this->inputData = $msData['data'];

			return $this->prepend(...$remainingMenuSvcs);	
		}

		$this->outputData = $this->inputData;
		return $this;	
		
	}


	/**
	 * Set the input.
	 * Prefereably called upon __construct()
	 * 
	 * @param array $input
	 * @return self
	 */
	public function setInputData(array $input) {
		$this->inputData = $input;
		return $this;
	}

	/**
	 * Set the code. 
	 * Preferrably, only to be used by
	 * prepend()
	 * 
	 * @param int $status
	 * @return self
	 */
	public function setCode(int $status) {
		$this->code = $status;
		return $this;
	}
	
	/**
	 * Response: prepare the response body.
	 * 
	 * @return array
	 */
	public function prepareResponse() {
		return [
			'code' => $this->code,
			'data' => $this->outputData
		];
	}

	/**
	 * Let the $inputData be processed by the
	 * prepended MenuServices
	 * 
	 * @param  self $menuServices,...
	 * @return self
	 */
	public function execute(self ...$menuServices){

		if ( ! empty($menuServices) ) {
			$prepended = $this->prepend( ...$menuServices )->prepareResponse();
			$this->inputData = $prepended['data'];
			$this->code = $prepended['code'];
		}
		
		if ($this->code === 0) {
			$this->processData();
		}

		return $this;
	}

	/**
	 * Process data: implement this method  to use the 
	 * $inputData to your will. This must also modify the
	 * $outputData and $code depending on the circumstance.
	 * 
	 * @return self
	 */
	abstract protected function processData();

}
