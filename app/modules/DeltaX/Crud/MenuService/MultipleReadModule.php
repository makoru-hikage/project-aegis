<?php

namespace DeltaX\Crud\MenuService;

use DeltaX\Crud\MenuService\MultipleReadService;
use DeltaX\Crud\SearchQuery\SearchFilterService;
use DeltaX\Crud\ContentRestriction\AuthorizationService;
use DeltaX\Crud\SearchQuery\SearchValidatorService;

use DeltaX\Crud\InputValidation\ValidationSet;
use DeltaX\Crud\ContentRestriction\UserIdentity;
use DeltaX\Crud\ModelService\CollectionModelService;

class MultipleReadModule {

	/**
	 * The data to be processed
	 * 
	 * @var array
	 */
	protected $inputData;

	/**
	 * The ModelService to be used 
	 * by MultipleReadService.
	 * The class name of the CollectionModelService
	 * 
	 * @var \DeltaX\Crud\ModelService\CollectionModelService
	 */
	protected $multipleReadSvc;

	/**
	 * The set of validation rules to be used.
	 * A class name may be used
	 * 
	 * @var \DeltaX\Crud\InputValidation\ValidationSet
	 */
	protected $validationSvc;

	/**
	 * The set of validation rules to be used.
	 * A class name may be used
	 * 
	 * @var \DeltaX\Crud\InputValidation\ValidationSet
	 */
	protected $authorizationSvc;

	/**
	 * ???
	 * 
	 * @var array
	 */
	protected $specialPermissions = ['admin', 'superuser'];
	
	public function __construct($data) {
		
		$this->setInputData($data);
		
	}

	public function setInputData(array $data = []){
		$this->inputData = $data;
		return $this;
	}

	public function setModelService(CollectionModelService $modelService){

		$this->multipleReadSvc = (new MultipleReadService($this->inputData))
			->setModelService($modelService);

		return $this;
	}

	public function prepareAuthorization(UserIdentity $userIdentity = null){

		$this->authorizationSvc = new AuthorizationService($userIdentity, $this->specialPermissions);
		return $this;
	}

	public function prepareValidation(ValidationSet $validationSet){
		$this->validationSvc = new SearchValidatorService($validationSet);
		return $this;
	}

	public function run(){

		$searchFilterSvc = new SearchFilterService;

		$menuServices = [ $searchFilterSvc, $this->validationSvc ];

		if ( $this->authorizationSvc instanceof AuthorizationService ){
			$menuServices[] = $this->authorizationSvc;
		}

		return $this->multipleReadSvc
			->execute(...$menuServices)
			->prepareResponse();
	}


}