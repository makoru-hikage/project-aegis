<?php

namespace DeltaX\Crud\MenuService;

use \DeltaX\Crud\SearchQuery\SearchFilter;
use \DeltaX\Exceptions\NullItemException;
use \DeltaX\Crud\ModelService\CollectionModelService;

class MultipleReadService extends MenuService {
	
	/**
	 * The model service to be used by this.
	 * 
	 * @var \DeltaX\Crud\ModelService\ModelService|string
	 */
	protected $modelService;

	/**
	 * Set the ModelService to be used
	 * 
	 * @param CollectionModelService $modelService 
	 * @return self
	 */
	public function setModelService(CollectionModelService $modelService){

		$this->modelService = $modelService;
		return $this;
	}

	/**
	 * Run the query
	 * 
	 * @return array
	 */
	protected function processData() {

		$input = $this->searchFilter;
	
		$collectionModelSvc = $this->modelService;

		$collectionModelSvcData = $collectionModelSvc
			->setSearchFilter($input)
			->getPreparedOutputData();

		$this->outputData = $collectionModelSvcData;
		$this->code = 200;
			
	}

	protected function dealWithExceptions(){
		
	}
}