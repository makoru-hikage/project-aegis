<?php

namespace DeltaX\Crud\InputValidation;

use DeltaX\Crud\InputValidation\InputValidationInterface;

abstract class ValidationSet implements InputValidationInterface {

	
	/**
	 * An array of validation rules grouped per the 
	 * columns of a model.
	 *
	 * @var array
	 */
	protected $validationRules = [];

	/**
	 * The constructor
	 *
	 * @return $this
	 */
	public function __construct(){
		$this->loadRules();
	}

	/**
	 * Since Validation Rules are not static values
	 * The developer must make a statement where one must
	 * assign an array of Validation Rules. The name of keys 
	 * must correspond to the attributes of an entity 
	 *
	 */
	abstract protected function loadRules();

	/**
	 * Get all or one of the Validation Rules
	 *
	 * @param string $field
	 * @return array|string
	 */
	public function getValidationRules($field = null){
		
		return $field ? 
			$this->validationRules[$field] : 
			$this->validationRules;
		
	}

	/**
	 * Returns true or an array of error messages upon validation.
	 * Accepts only one field if field is supplied
	 *
	 * @param array $input
	 * @param string $field
	 * @return bool|array
	 */
	public function validate(array $input){
		
		$errorMessages = [];

		foreach ($input as $key => $value) {

			//The keys must match the keys of this object's $validationRules' keys
			//or they'll be ignored.
			if (! isset($this->validationRules[$key]) ) {
				continue;
			}

			$isValid = $this->validationRules[$key]->validate($value);
				
			if ($isValid !== true) {
				$errorMessages[$key] = $isValid;
			}

		}

		return $errorMessages;
	}

}