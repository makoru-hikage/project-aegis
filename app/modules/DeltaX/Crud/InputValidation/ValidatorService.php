<?php

namespace DeltaX\Crud\InputValidation;

use DeltaX\Crud\MenuService\MenuService;
use DeltaX\Crud\InputValidation\InputValidationInterface;

class ValidatorService extends MenuService {

	/**
	 * An object to validate input for queries 
	 * 
	 * @var \DeltaX\Crud\InputValidation\ValidatorInterface $validator
	 */

	protected $validator;

	/**
	 * A Validator is required
	 * 
	 * @param \DeltaX\Crud\InputValidation\InputValidationInterface $validator
	 */
	public function __construct(InputValidationInterface $validator){
		$this->setValidator($validator);
	}

	/**
	 * Sets the Validator object to be used
	 * 
	 * @param \DeltaX\Crud\InputValidation\InputValidationInterface $validator
	 * @return self
	 */
	public function setValidator(InputValidationInterface $validator) {
		$this->validator = $validator;
		return $this;
	}

	/**
	 * Validate the input
	 * 
	 * @return self
	 */
	protected function validate() {

		$input = $this->inputData;

		//If validate() yields 'true', all inputs are valid
		$inputIsValid = $this->validator->validate($input);

		//validate() returns an array of errors, empty means no issues
		if (count($inputIsValid) > 0) {

			$this->code = 400;
			$this->outputData = $inputIsValid;

			return $this;
		}
		
		return $this;
	}

	protected function processData(){
		$this->validate();

		return $this;
	}
}