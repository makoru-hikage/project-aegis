<?php

namespace DeltaX\Crud\InputValidation;

use Respect\Validation\Validator;
use Respect\Validation\Exceptions\NestedValidationException;
use DeltaX\Crud\InputValidation\ValidationRules;

class RespectValidationAdapter extends ValidationRules {

	public function __construct(Validator $validator) {
		$this->validator = $validator;
	}

	public static function use(Validator $validator){
		return new self ($validator);
	}

	public function validate($input){

		try {
			return $this->validator->assert($input);
		} catch (NestedValidationException $e){
			return $e->getMessages();
		}

	}


}