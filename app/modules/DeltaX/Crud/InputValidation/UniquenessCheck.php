<?php

namespace DeltaX\Crud\ModelService;

use DeltaX\Crud\ModelService\ModelService;

class UniquenessCheck extends ModelService {

	/**
	 * See if a particular database entry exists
	 * 
	 * @param  string $tableName
	 * @param  array|string $params   
	 * @param  mixed $value    
	 * @return Illuminate\Database\Eloquent\Model|mixed|null       
	 */
	public function presenceCheck(string $tableName, $param, $value = null, $columns = ['*']){

		switch ($param) {
			case ! empty($param) && is_array($param) :
				return $this->repoInvoker->find($tableName, $param, $value, $columns);
				break;

			case is_string($param) && $value !== null :
				return $this->repoInvoker->find($tableName, $param, $value, $columns);
				break;
			
			default:
				return null;
				break;
		}
	}




} 