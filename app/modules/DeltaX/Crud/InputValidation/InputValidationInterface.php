<?php

namespace DeltaX\Crud\InputValidation;

interface InputValidationInterface{
	public function validate(array $input);
	public function getValidationRules($field);
}