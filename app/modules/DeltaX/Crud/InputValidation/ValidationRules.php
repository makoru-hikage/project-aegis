<?php

namespace DeltaX\Crud\InputValidation;

abstract class ValidationRules {

	protected $validator; 

	public function __construct($validator) {
		$this->validator = $validator;
	}

	/**
	 * Validate an input.
	 * An empty array or true means passed
	 * 
	 * @param  $input 
	 * @return array|bool        
	 */
	abstract public function validate($input);

}