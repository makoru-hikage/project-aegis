<?php

namespace DeltaX\Crud\ContentRestriction;

use DeltaX\Crud\ModelService\ModelService;
use DeltaX\UserAuthentication\UserAuthenticationAdapterInterface;
use DeltaX\Exceptions\NoLoggedInUserException;
use DeltaX\Crud\ModelService\ModelRepository;

/**
 * UserIdentity
 */

abstract class UserIdentity extends ModelService {


	/**
	 * The user data
	 * 
	 * @var array|object|null
	 */
	protected $user;

	/**
	 * Useful when defining roles
	 * 
	 * @var string
	 */
	protected $userType = 'users';

	/**
	 * The unique key that identifies an identity
	 * @var string
	 */
	protected $identityKey = 'sample_username';	

	/**
	 * An object to authenticate an authorized user
	 * @var DeltaX\ContentAccess\UserAuthenticationAdapterInterface;
	 */
	protected $authenticator;

	
	/**
	 * __construct
	 * self
	 */
	public function __construct(
		UserAuthenticationAdapterInterface $authenticator, 
		ModelRepository $repo,
		$identityKey = null
	) {
		try {
			parent::__construct($repo);
			$this->setAuthenticator($authenticator)->setUserData();

		} catch (NoLoggedInUserException $e) {

			$this->userType = 'guest';
			
		}

		if ($identityKey) {
			$this->setIdentityKey = $identityKey;
		}

	}

	/**
	 * Sets the object used for authenticator
	 *
	 * @param \DeltaX\UserAuthentication\UserAuthenticationAdapterInterface $authenticator
	 * @return self
	 */
	public function setAuthenticator(UserAuthenticationAdapterInterface $authenticator) {	
		$this->authenticator = $authenticator;
		return $this;
	}

	/**
	 * Authorize based on permission
	 * 
	 * @return bool
	 */
	public function hasAnyAccess($permissions) {

		if ($this->userType === 'guest'){
			return false;
		}
		
		return $this->authenticator->authorize($permissions);

	}

	/**
	 * setUserData
	 * @return self
	 */
	protected function setUserData() {

		$this->user = $this->authenticator->getUser();

		if (! $this->user){
			throw new NoLoggedInUserException;
		}

		return $this;
	}

	public function getIdentityData(){

		$details = $this->userType === 'guest' ?: $this->determineIdentity();

		return [
			'type' => $this->userType,
			'details'=> $details
		];	
	}

	/**
	 * 
	 * @return string
	 */
	public function getUserType(){
		return $this->userType;
	}

	/**
	 * 
	 * @return string
	 */
	public function getIdentityKey(){
		return $this->identityKey;
	}

	/**
	 * The value to be used by 
	 * determineIdentity method
	 * 
	 * @return string
	 */
	public function setIdentityKey($key){
		
		$this->identityKey = $key;
		return $this;

	}

	/**
	 * 
	 * 
	 * @return array|null
	 */
	abstract protected function determineIdentity();

}