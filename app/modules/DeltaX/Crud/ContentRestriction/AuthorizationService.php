<?php

namespace DeltaX\Crud\ContentRestriction;

use DeltaX\Crud\MenuService\MenuService;

class AuthorizationService extends MenuService {


	/**
	 * The search filter involved with user identity
	 * 
	 * @var array
	 */
	protected $userIdentity;

	/**
	 *
	 * 
	 * @var array
	 */
	protected $permissions = [];

	/**
	 * All MenuService are available to
	 * the 'superuser'
	 *
	 * @param \DeltaX\Aegis\ModelServices\UserManagement\UserIdentity
	 * @return self
	 */
	public function __construct(UserIdentity $identity, array $permissions = []){

		$this->permissions = $permissions;
		$this->userIdentity = $identity;
		
	}

	protected function processData(){

		$authorized = $this->verticallyAuthorize() || $this->horizontallyAuthorize();

		$this->code = $authorized ? 0 : 401;
		$this->outputData = $authorized ? $this->inputData : ['message' => 'Access denied'];
		
		return $this;
	}

	/**
	 * Authorize content by checking the search filter
	 * against the identity data
	 * 
	 * @return bool
	 */
	protected function horizontallyAuthorize(){

		$filteredItems = $this->inputData;
		$identityData = $this->userIdentity->getIdentityData();

		if ($identityData['type'] === 'guest') {
			return false;
		}

		$invalidCredentials = [];

		foreach ($identityData as $key => $item) {

			if ( isset($filteredItems[$key]) && $item !== $filteredItems[$key]){
				$invalidCredentials[$key] = $item;
			}

		}

		$authorized = count($invalidCredentials) < 0 || ! empty($identityData);

		return $authorized;
	}

	/**
	 * Authorize based on the level of position
	 * 
	 * @return bool
	 */
	protected function verticallyAuthorize(){

		return $this->userIdentity->hasAnyAccess($this->permissions);

	}
}