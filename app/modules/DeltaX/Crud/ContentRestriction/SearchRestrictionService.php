<?php

namespace DeltaX\Crud\ContentRestriction;

class SearchRestrictionService extends AuthenticatorService{

	/**
	 * Create a restricted identity filter if it does not exist
	 *  
	 * @return self
	 */
	protected function authorize(){

		$inputData = $this->inputData;

		if (is_null($inputData)) {
			$identityData = $this->userIdentity->getIdentityData()['details'];

			foreach ($identityData as $key => $value) {
				$this->inputData[] = [$key, '=', $value];
			}
		}
		
		return $this;
	}

}