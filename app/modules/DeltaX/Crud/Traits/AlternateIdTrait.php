<?php

namespace DeltaX\Crud\Traits;

trait AlternateIdTrait{

	public function getAlternateIdKey(){
		return $this->alternateIdKey;
	}

	public function findByAlternateId($id){
		try {
			return $this->where($this->alternateIdKey, $id)->firstOrFail();
		} catch (Illuminate\Database\Eloquent\ModelNotFoundException $e) {
			return null;
		}
	}
}