<?php

namespace DeltaX\Aegis\ModelServices\CourseSessionRegistry;

use DeltaX\Crud\ModelService\ModelService;

class CourseSessionManager extends ModelService {


	/**
	 * courseTaught
	 * @var \DeltaX\Models\Course
	 */

	protected $courseTaught;


	/**
	 * instructor
	 * @var \DeltaX\Models\Employee
	 */

	protected $instructor;


	/**
	 * term
	 * @var \DeltaX\Models\SchoolCalendar
	 */

	protected $term;


	/**
	 * sessionAlias
	 * @var string
	 */

	protected $sessionAlias;


	/**
	 * assignInstructor
	 * @param string $employee_number
	 * @return self
	 */
	public function assignInstructor($employee_number) {
		$instructor = $this->repoInvoker->find('employees', $employee_number);

		if (empty($instructor)){
			throw new InvalidEmployeeNumberException;
		}

		$this->instructor = $instructor;
		return $this;
	}

	/**
	 * setCourse
	 * @param $code
	 * @return self
	 */
	public function setCourse($code) {
		$course = $this->repoInvoker->find('courses', $code);

		if (empty($course)){
			throw new InvalidCourseCodeException;
		}

		$this->courseTaught = $course;
		return $this;
	}

	/**
	 * setTerm
	 * @param int $year_start
	 * @param string $semester
	 * @return self
	 */
	public function setTerm($year_start, $semester) {
		$term = $this->repoInvoker
			->entity('school_calendar', function($entity) use ($year_start, $year_end, $semester){
				return $entity->whereSemesterAndAcademicyear($year_start, $semester);
		})->get()->first();

		if (empty($term)){
			throw new InvalidTermException;
		}

		$this->term = $term;
		return $this;
	}

	/**
	 * setCode
	 * @param string $degreeCode
	 * @param string $year
	 * @param string $section
	 * @return self
	 */
	public function setAlias($degreeCode, $year, $section = '') {
		$this->code = $value ?? $this->term->code . "_" . $degreeCode . $year . $section;
		return $this;
	}

	/**
	 * Creates a class session
	 * 
	 * @return \DeltaX\Models\CourseSession
	 */
	public function createSession() {
		$params = [];
		$params['course_id'] = $this->courseTaught->id;
		$params['employee_id'] = $this->instructor->id;
		$params['school_calendar_id'] = $this->term->id;
		$params['sessionAlias'] = $this->sessionAlias;

		$this->outputData = $this->repoInvoker->create('course_sessions', $params);
		return $this;
	}

	/**
	 * Reassigns the designated instructor in a class
	 *
	 * @param string $employeeNumber
	 * @param string|\DeltaX\Models\CourseSession $session
	 * @return \DeltaX\Models\CourseSession
	 */
	public function reassignInstructor($employeeNumber, $session){
		if ( ! $session instanceof CourseSession ){
			$session = $this->repoInvoker->find('course_sessions', $session);
		}

		$this->assignInstructor($employeeNumber);
		$session->employee_id = $this->instructor->id;
		$session->save();

		$this->outputData = $session;
		return $this;
	}

}