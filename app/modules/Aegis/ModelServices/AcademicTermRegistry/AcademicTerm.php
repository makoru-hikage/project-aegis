<?php

use DeltaX\Crud\Model\ModelService;

class AcademicTerm extends ModelService {
	public function listTerms(){
		$this->repoInvoker = $this->repoInvoker->entity('school_calendar');
		$this->outputData = $this->repoInvoker->get();
	}
}