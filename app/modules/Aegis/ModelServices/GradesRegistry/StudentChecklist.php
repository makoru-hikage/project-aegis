<?php

namespace DeltaX\Aegis\ModelServices\GradesRegistry;

use DeltaX\Crud\ModelService\CollectionModelService;
use DeltaX\Aegis\Exceptions\InvalidStudentNumberException;
use Carbon\Carbon;
use DeltaX\Utilities as U;

class StudentChecklist extends CollectionModelService {

	/**
	 * A blank checklist shows a curriculum
	 * 
	 * @var string
	 */
	protected $mainTable = 'curricula';

	/**
	 * 
	 * 
	 * @var array
	 */
	protected $specialParams = [
		'enrolled_courses.student_id'
	];

	/**
	 * To be used in WHERE clause
	 * 
	 * @var array
	 */
	protected $paramsWithFullName = [
		'course_code' => 'courses.code',
		'course_name' => 'courses.name',
		'credits_in_lecture' => 'courses.credits_in_lecture',
		'credits_in_lab' => 'courses.credits_in_lab',
		'division_type' => 'curriculum_terms.division_type',
		'lec_contact_hrs' =>'curricula.lec_contact_hrs',
		'lab_contact_hrs' =>'curricula.lab_contact_hrs',
		'year_level' =>'curriculum_terms.year_level',
		'part_number' => 'curriculum_terms.part_number',
		'instructor_fname' => 'instructors.first_name',
		'instructor_surname' => 'instructors.last_name',
		'final_grade' => 'enrolled_courses.final_grade',
	];

	/**
	 * setDegree
	 * @param string $code
	 * @return self
	 */
	public function setStudent(string $studentNumber) {

		$student = $this->repoInvoker->find('students', 'student_number', $studentNumber);


		if (! $student ){
			throw new InvalidStudentNumberException;
		}

		return $this->addFilterItem(['enrolled_courses.student_id', '=', $student->id]);

	}

	/**
	 * 
	 * 
	 * @return self
	 */
	protected function fetchDataFromForeignTables(){

		$student_number = $this->searchFilter->getFilter('student_number')->getValue();
		$this->setStudent($student_number);
		$this->searchFilter->removeFilter('student_number');
		
		return $this;

	}

	/**
	 * 
	 * 
	 * @return self
	 */
	protected function prepareColumns(){

		$this->repoInvoker->modifyEntity(function($entity){

			$entity = $entity
				->select([
					'courses.code',
					'courses.name',
					'courses.credits_in_lecture',
					'courses.credits_in_lab'
				])->selectRaw('courses.credits_in_lab + courses.credits_in_lecture AS total_credits')
				->addSelect([
					'curricula.lec_contact_hrs',
					'curricula.lab_contact_hrs',
					'curricula.is_nth_yr_standing',
					'curriculum_terms.year_level',
					'curriculum_terms.part_number',
				])->selectRaw('GROUP_CONCAT(prerequisite_courses.code) AS prerequisite')
				->addSelect([
					'semester_attended.date_start',
					'semester_attended.part_number',
					'semester_attended.division_type',
					'instructors.first_name',
					'instructors.last_name',
					'enrolled_courses.final_grade'
				]);

			return $entity;

		});

		return $this;
	}

	/**
	 * runQuery
	 * @return \Illuminate\Database\Eloquent\Collection|null
	 */
	protected function setTableJoins() {

		$this->repoInvoker->modifyEntity(function($entity){

			return $entity
				->join('courses', 'curricula.course_id', '=', 'courses.id')
				->join('curriculum_terms', 'curricula.curriculum_term_id', '=', 'curriculum_terms.id')
				->leftJoin('curriculum_prerequisites', 'curriculum_prerequisites.dependent_id', '=', 'curricula.id')
				->leftJoin('curricula AS prerequisites', 'curriculum_prerequisites.prerequisite_id', '=', 'prerequisites.id')
				->join('courses AS prerequisite_courses','curricula.course_id', '=', 'prerequisite_courses.id')
				->leftJoin('enrolled_courses', 'enrolled_courses.curriculum_id', '=', 'curricula.id')
				->join('term_attendance', 'enrolled_courses.term_attendance_id', '=', 'term_attendance.id')
				->join('school_calendar AS semester_attended', 'term_attendance.school_calendar_id', '=', 'semester_attended.id')
				->leftJoin('remarks', 'enrolled_courses.remark_id', '=', 'remarks.id')
				->leftJoin('course_sessions', 'enrolled_courses.course_session_id', '=', 'course_sessions.id')
				->leftJoin('employees', 'course_sessions.employee_id', '=', 'employees.id')
				->leftJoin('users AS instructors', 'employees.user_id', '=', 'instructors.id')
				->groupBy('curricula.id');

		});

		return $this;

	}

	protected function createInstructorColumn($item){

		$initial = substr($item['first_name'], 0, 1) . '.';
		$item['instructor'] = $initial . ' ' . $item['last_name'];
		unset($item['first_name']);
		unset($item['last_name']);

		return $item;
		
	}

	protected function createTermTakenColumn($item){

		$termTypes = [
			'semester' => 6,
			'trimester' => 4,
			'quarter' => 3
		];

		$partNumber = $item['part_number'];
		$numberOfMonths = $termTypes[$item['division_type']];

		$subtrahend = ($partNumber == 1) ?
			0 : ($partNumber * $numberOfMonths);

		$yearStart = Carbon::parse($item['date_start'])
			->subMonth($subtrahend)->year;

		$yearEnd = $yearStart + 1;
		$nthTerm = U\ordinal($item['part_number']);

		//(e.g. 2nd, 2012-2013)
		$item['term_taken'] = $nthTerm . ', ' . $yearStart . '-' .$yearEnd;

		unset($item['part_number']);
		unset($item['division_type']);
		unset($item['date_start']);

		return $item;

	}

	protected function createPrerequisiteColumn($item){
		
		if ($item['is_nth_yr_standing']){
			$item['prerequisite'] = U\ordinal($item['year_level']) . ' Year standing';
		}
		unset($item['is_nth_yr_standing']);
		unset($item['year_level']);
		return $item;

	}

	/**
	 * 
	 * @return self
	 */
	protected function customClause(){

		return $this->setTableJoins();

	}

	public function finalizeOutput($item){

		return $this->createTermTakenColumn(
			$this->createInstructorColumn(
				$this->createPrerequisiteColumn(
					$item
				)
			)
		);
	}

	protected function modifyOutputData($data){


		return $data->map( [$this, 'finalizeOutput'] );

	}

}
