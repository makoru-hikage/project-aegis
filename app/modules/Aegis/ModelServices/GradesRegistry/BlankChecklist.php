<?php

namespace DeltaX\Aegis\ModelServices\GradesRegistry;

use DeltaX\Crud\ModelService\CollectionModelService;
use DeltaX\Utilities as U;
use DeltaX\Aegis\Exceptions\InvalidDegreeException;

class BlankChecklist extends CollectionModelService {

	/**
	 * A blank checklist shows a curriculum
	 * 
	 * @var string
	 */
	protected $mainTable = 'curricula';

	/**
	 * To be used in WHERE clause
	 * 
	 * @var array
	 */
	protected $paramsWithFullName = [
		'course_code' => 'courses.code',
		'course_name' => 'courses.name',
		'credits_in_lecture' => 'courses.credits_in_lecture',
		'credits_in_lab' => 'courses.credits_in_lab',
		'lec_contact_hrs' =>'curricula.lec_contact_hrs',
		'lab_contact_hrs' =>'curricula.lab_contact_hrs',
		'year_level' =>'curriculum_terms.year_level',
		'part_number' => 'curriculum_term.part_number',
	];

	/**
	 * 
	 * 
	 * @var array
	 */
	protected $specialParams = [
		'degree_code',
	];

	/**
	 * Curriculum of what degree program?
	 * 
	 * @param string $code
	 * @return self
	 */
	public function setDegree(string $code) {
		$degree = $this->repoInvoker->find('degrees', 'code' ,$code);
		if (empty($degree)){
			throw new InvalidDegreeException ("Degree program, $code, does not exist");
		}

		$this->addFilterItem(['curricula.degree_id', '=', $degree->id]);
		
		return $this;
	}

	/**
	 *
	 * @return  self
	 */
	protected function setTableJoins(){

		$this->repoInvoker->modifyEntity(function($entity){

			$entity = $entity
				->join('courses', 'curricula.course_id', '=', 'courses.id')
				->join('curriculum_terms', 'curricula.curriculum_term_id', '=', 'curriculum_terms.id')
				->leftJoin('curriculum_prerequisites', 'curriculum_prerequisites.dependent_id', '=', 'curricula.id')
				->leftJoin('curricula AS prerequisites', 'curriculum_prerequisites.prerequisite_id', '=', 'prerequisites.id')
				->leftJoin('courses AS prerequisite_courses','prerequisites.course_id', '=', 'prerequisite_courses.id')
				->groupBy('curricula.id');

			return $entity;
		});

		return $this;
	} 

	/**
	 * 
	 * 
	 * @return self
	 */
	protected function prepareColumns(){

		$this->repoInvoker->modifyEntity(function($entity){

			$entity = $entity
				->select([
					'courses.code',
					'courses.name',
					'courses.credits_in_lecture',
					'courses.credits_in_lab'
				])->selectRaw('courses.credits_in_lab + courses.credits_in_lecture AS total_credits')
				->addSelect([
					'curricula.lec_contact_hrs',
					'curricula.lab_contact_hrs',
					'curricula.is_nth_yr_standing',
					'curriculum_terms.year_level'
				])->selectRaw('GROUP_CONCAT(prerequisite_courses.code) AS prerequisite');

			return $entity;

		});

		return $this;
	}

	/**
	 *
	 * 
	 * @return self
	 */
	protected function customClause(){

		return $this->setTableJoins();

	}

	/**
	 * 
	 * 
	 * @return self
	 */
	protected function fetchDataFromForeignTables(){

		$degree_code = $this->searchFilter->getFilter('degree_code')->getValue();
		$this->setDegree($degree_code);
		$this->searchFilter->removeFilter('degree_code');
		
		return $this;

	}

	protected function createPrerequisiteColumn($data){
		$outputData = $data->map(function ($item){
			if ($item['is_nth_yr_standing']){
				$item['prerequisite'] = U\ordinal($item['year_level']) . ' Year standing';
			}
			unset($item['is_nth_yr_standing']);
			unset($item['year_level']);
			return $item;
		});

		return $outputData;
	}

	protected function modifyOutputData($data){
		return $this->createPrerequisiteColumn($data);
	}

}