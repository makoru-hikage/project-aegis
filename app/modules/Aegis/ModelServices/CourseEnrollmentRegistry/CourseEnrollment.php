<?php

namespace DeltaX\Aegis\ModelServices\CourseEnrollmentRegistry;

use DeltaX\Crud\ModelService\ModelService;

class CourseEnrollment extends ModelService {


	/**
	 * student
	 * @var \DeltaX\Models\Student
	 */

	protected $student;


	/**
	 * courseEnrolled
	 * @var \DeltaX\Models\Curriculum
	 */

	protected $courseEnrolled;


	/**
	 * sessionAttended
	 * @var \DeltaX\Models\CourseSession
	 */

	protected $sessionAttended;


	/**
	 * termRegistered
	 * @var \DeltaX\Models\TermAttendance
	 */

	protected $termRegistered;

	/**
	 * remark
	 * @var \DeltaX\Models\TermAttendance
	 */

	protected $remark;

	/**
	 * enrolledCourse
	 * @var \DeltaX\Models\EnrolledCourse
	 */

	protected $enrolledCourse;


	/**
	 * setStudent
	 * @param string $studentNumber
	 * @return $this
	 */
	public function setStudent(string $studentNumber) {
		$this->student = $this->repoInvoker->find('students', $studentNumber);

		if (empty($this->student)) {
			throw new NullStudentException;
		}

		return $this;
	}

	/**
	 * setCourseEnrolled
	 * @param string $code
	 * @return $this
	 */
	public function setCourseEnrolled(string $code) {
		$this->student = $this->repoInvoker
			->find('course', $studentNumber)
			->curriculum()
			->where('curriculum.degree_id', $this->student->degree_id)
			->get()->first();

		return $this;
	}

	/**
	 * setSessionAttended
	 * @param string $sessionAlias
	 * @return $this
	 */
	public function setSessionAttended($code) {
		$this->sessionAttended = $this->repoInvoker->find('course_session', $code);
		return $this;
	}

	/**
	 * setTermRegistered
	 * @param string $code
	 * @return $this
	 */
	public function setTermRegistered(string $code) {
		$term = $this->repoInvoker->find('term_attendance', $code);
		return $this;
	}

	/**
	 * setRemark
	 * @param string $remark
	 * @return $this
	 */
	public function setRemark(string $remark) {
		$this->repoInvoker->find('remarks', $remark);
		return $this;
	}

	/**
	 * enrollCourse
	 * @param  bool $checkEligibility
	 * @return \DeltaX\Models\EnrolledCourse
	 */
	public function enrollCourse($checkEligibility = true) {
		$params = [
			'student_id' => $this->student->id,
			'curriculum_id' => $this->courseEnrolled->id,
			'course_session_id' => $this->sessionAttended->id,
			'term_attendance_id' => $this->termRegistered->id
		];

		$this->outputData = $this->repoInvoker->create('enrolled_courses', $params);
		return $this;
	}

	/**
	 * loadEnrolledCourse
	 * @return \DeltaX\Models\EnrolledCourse 
	 */
	public function loadEnrolledCourse() {
		$params = [
			['student_id',  $this->student->id],
			['curriculum_id',  $this->courseEnrolled->id],
			['course_session_id',  $this->sessionAttended->id],
			['term_attendance_id',  $this->termRegistered->id]
		];

		$this->enrolledCourse = $this->repoInvoker
			->entity('enrolled_courses')
			->where($params)
			->get()
			->first();

		return $this;

	}

	/**
	 * dropCourse
	 * @return \DeltaX\Models\EnrolledCourse 
	 */
	public function dropCourse($recorded = true) {

		$this->setRemark('DROPPED');

		$data = $recorded ? 
			['remark_id' => $this->remark->id, 'final_grade' => 5.00 ] : 
			['is_deleted' => 1];

		$this->outputData = $this->repoInvoker
			->data($data)
			->update($this->enrolledCourse);

		return $this;
	}

	/**
	 * setFinalGrade
	 * @return double $grade
	 * @return string|null $remark
	 * @return \DeltaX\Models\EnrolledCourse 
	 */
	public function setFinalGrade($grade, $remark = null) {

		$data = ['final_grade' => $grade, 'remark_id' => null];

		if (! $customRemark) {
			$remark = ($grade <= 3.00) ? 'PASSED' : 'FAILED';
		}

		$this->setRemark($remark);
		$data['remark_id'] = $this->remark->id;

		$this->outputData = $this->repoInvoker
			->data($data)
			->update($this->enrolledCourse);

		return $this;
	}

}