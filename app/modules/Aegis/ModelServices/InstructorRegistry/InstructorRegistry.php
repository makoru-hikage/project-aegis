<?php 

namespace DeltaX\Aegis\ModelServices\InstructorRegistry;

use DeltaX\Crud\ModelService\ModelService;

class InstructorRegistry extends ModelService {


	/**
	 * The instructor's user info.
	 * 
	 * @var \DeltaX\Models\User
	 */
	protected $user;

	/**
	 * The student's user details see User Model
	 * 
	 * @var array
	 */
	protected $userAttributes = [];

	/**
	 * instructorAttributes
	 * @var array
	 */

	protected $instructorAttributes = [];

	/**
	 * Sets the User by username
	 * 
	 * @param string $username
	 * @return self
	 */
	public function setUser($username) {
		$params = $this->userAttributes;
		$this->user = $this->repoInvoker->updateOrCreate('user', $username, $params);
		return $this;
	}

	/**
	 * Set attributes per the Employee Model
	 * 
	 * @param array $value
	 * @return self
	 */
	public function setInstructorAttributes(array $params) {
		$this->instructorAttributes = $params;
		return $this;
	}

	/**
	 * Create the Instructors account
	 * 
	 * @return \DeltaX\Models\Employee
	 */
	public function createEmployeeAccount() {
		$params = $this->instructorAttributes;
		$params['user_id'] = $this->user->id;

		$employee = $this->repoInvoker->create('employee', $params);
		$this->outData = $employee;
		return $this;
	}

}