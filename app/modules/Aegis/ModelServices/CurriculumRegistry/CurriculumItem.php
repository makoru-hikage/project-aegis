<?php

namespace DeltaX\Aegis\ModelServices\CurriculumRegistry;

use DeltaX\Crud\ModelService\ModelService;
/**
 * CurriculumItem
 */

class CurriculumItem extends ModelService {


	/**
	 * term
	 * @var \DeltaX\Models\CurriculumTerm
	 */

	protected $term;


	/**
	 * course
	 * @var \DeltaX\Models\Course
	 */

	protected $course;

	/**
	 * degree
	 * @var \DeltaX\Models\Degree
	 */

	protected $degree;


	/**
	 * 
	 * @var \DeltaX\Models\Collection
	 */

	
	protected $prerequisites = null;


	/**
	 * curriculumItemAttributes
	 * @var array
	 */

	protected $curriculumItemAttributes = [
		'code'=> null,
		'lec_contact_hrs' => 0,
		'lab_contact_hrs' => 0,
		'is_major' => 0,
		'is_nth_yr_standing' => 0
	];

	/**
	 * setCurriculumTerm
	 * @param int $yearLevel
	 * @param string $termType
	 * @param int $termPart
	 * @return self
	 */
	public function setCurriculumTerm(int $yearLevel, string $termType, int $termPart) {
		$params = [
			['year_level' , $yearLevel],
			['division_type' , $termType],
			['part_number' , $termPart]
		];

		$term = $this->repoInvoker
			->entity('curriculum_terms')
			->where($params)
			->get()->first();

		if (empty($term)) {
			throw new InvalidCurriculumTermException;
		}

		$this->term = $term;
		return $this;
	}

	/**
	 * setCurriculumCode
	 * @param string $code
	 * @return self
	 */
	public function setCurriculumCode($code) {
		$this->curriculumItemAttributes['code'] = $code;
		return $this;
	}

	/**
	 * setCourse
	 * @param string $code
	 * @return self
	 */
	public function setCourse(string $code) {
		$course = $this->repoInvoker->first('courses', $code);

		if (empty($course)) {
			throw new InvalidCourseCodeException;
		}

		$this->course = $course;
		return $this;
	}

	/**
	 * setDegree
	 * @param string $code
	 * @return self
	 */
	public function setDegree(string $code) {
		$degree = $this->repoInvoker->first('degrees', $code);

		if (empty($degree)) {
			throw new InvalidDegreeCodeException;
		}

		$this->degree = $degree;
		return $this;
	}

	/**
	 * setHours
	 * @param int $lectureHours
	 * @param int $labHours
	 * @return self
	 */
	public function setHours(int $lectureHours, int $labHours) {
		$this->curriculumItemAttributes['lec_contact_hrs'] = $lectureHours;
		$this->curriculumItemAttributes['lab_contact_hrs'] = $labHours;
		return $this;
	}

	/**
	 * toggleMajor
	 * @param bool $value
	 * @return self
	 */
	public function setMajor(bool $value) {
		$this->curriculumItemAttributes['is_major'] = (int)$value;
		return $this;
	}

	/**
	 * toggleNthYrStanding
	 * @param bool $value
	 * @return self
	 */
	public function setNthYrStanding(bool $value) {
		$this->curriculumItemAttributes['is_nth_yr_standing'] = (int)$value;
		return $this;
	}


	/**
	 * setPrerequisites
	 * @param array $courseCodes
	 * @return self
	 */
	public function setPrerequisites($courseCodes){
		$courseIdKeys = $this->repoInvoker
			->entity('courses', function ($entity) use ($courseCodes){
				return $entity->select(['id'])->whereIn('code', $courseCodes);
			})->pluck('id');

		$this->prerequisites = $this->repoInvoker
			->entity('curricula', function($entity){
				return $entity->whereIn('course_id', $courseIdKeys);
			})->get();

		return $this;
	}

	/**
	 * createCurriculumItem
	 * @return \Delta\Models\Curriculum
	 */
	public function createCurriculumItem() {
		$params = $this->curriculumItemAttributes;
		$params['curriculum_term_id'] = $this->term->id;
		$params['course_id'] = $this->course->id;

		$this->outputData = $this->repoInvoker->create('curriculum', $params);
		return $this;
	}

}