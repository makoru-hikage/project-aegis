<?php


/**
 * 
 */

class InstructorIdentity extends UserIdentity {

	protected $userType = 'Instructor';

	/**
	 * getstudentIdentity
	 * @return self
	 */
	public function determineIdentity() {

		$columns = [
			'employees.employee_number',
			'users.last_name',
			'users.first_name',
			'users.middle_name',
			'users.sex'
		];

		$this->repoInvoker->entity('user', function($entity){
			return $entity
				->join('employees', 'employees.user_id', '=', 'users.id');
		})->columns($columns)
		->first(['users.id', $this->user->id]);
	}

	public function getAdvisees(){
		$this->repoInvoker->entity('user', function($entity){
			return $entity
				->join('employees', 'employees.user_id', '=', 'users.id');
				->join('student', 'students.adviser_id', '=', 'employees.id');
				->join('users', 'students.user_id', '=', 'users.id');
		})->columns($columns)->get();
	}

}