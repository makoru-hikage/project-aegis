<?php

namespace DeltaX\Aegis\ModelServices\UserManagement;

use DeltaX\Crud\ContentRestriction\UserIdentity;
/**
 * UserIdentityCheck
 */

class StudentIdentity extends UserIdentity {

		
	protected $userType = 'students';

	/**
	 * getIdentity
	 * @return self
	 */
	protected function determineIdentity() {

		$columns = [
			'students.student_number',
			'users.last_name',
			'users.first_name',
			'users.middle_name',
			'degrees.code',
			'students.classification',
		];

		
		$student_number = $this->identityKey;
		$userId = $this->user->id;

		return $this->repoInvoker->entity('users', function($entity) use ($columns, $student_number, $userId){
			return $entity
				->select($columns)
				->join('students', 'students.user_id', '=', 'users.id')
				->join('degrees', 'students.degree_id', '=', 'degrees.id')
				->where('users.id', $userId)				
				->where('students.student_number', $student_number);
		})->get()->toArray();
	}

}