<?php

namespace DeltaX\Aegis\ModelServices\StudentRegistry;

use DeltaX\Crud\ModelService\ModelService;

class StudentEnrollment extends ModelService {
	
	protected $student;
	
	protected $term;

	protected $enrollmentStatus = [
		'year_level' => 0,
		'section' => null,
		'registration_status' => null
	];

	public function setStudent($studentNumber){
		$this->student = $this->repoInvoker->find('students', $studentNumber);

		if (empty($this->student)) {
			throw new NullStudentException;
		}

		return $this;
	}

	public function setYearLevelAndSection(int $year_level, $section = null){
		$this->enrollmentStatus['year_level'] = $year_level;
		$this->enrollmentStatus['section'] = $section;

		return $this;
	}

	public function setRegistrationStatus($status){
		if (in_array($status, ['Regular', 'Irregular', 'Dropped', 'Expelled', 'Transferred'])){
				throw new InvalidRegistrationStatusException;
		}
		$this->studentEnrollmentStatus['registration_status'] = $status;

		return $this;
	}

	public function setTerm($year_start, $year_end, $semester){
		$term = $this->repoInvoker
			->entity('school_calendar', function($entity) use ($year_start, $year_end, $semester){
				return $entity->whereSemesterAndAcademicyear($year_start, $semester);
		});

		$this->term = $term->get()->first();

		return $this;
	}

	public function shiftStudent($degreeCode, $newStudentNumber){
		if($newStudentNumber == $this->student_number){
				throw new InvalidStudentNumberException;
		}

		$this->student->classification = 'Shifted';
		$this->student->save();

		$newStudentInfo = $this->student->replicate(['id', 'student_number']);
		$newStudentInfo->degree_id = $this->repoInvoker->find('degrees', $degreeCode)->id;
		$newStudentInfo->student_number = $newStudentNumber;
		$newStudentInfo->save();

		$this->outputData = $newStudentInfo;
		return $this;
	}

	public function enrollStudent(){

		$params = $this->enrollmentStatus;
		$params['student'] = $this->student->id;
		$params['school_calendar_id'] = $this->term->id;

		$termAttendanceCode = $this->student->student_number . '_' . $this->term->code;
		$params['code'] = $termAttendanceCode;

		$this->outputData = $this->repoInvoker->create('term_attendance', $params);
		return $this;			

	}

	protected function changeStudentClassification(string $classification){
		$this->student->classification = $classification;
		$this->student->save();
		return $this->student;
	}

	public function enrollNewStudent(){
		$this->outputData = $this->changeStudentClassification("New");
		return $this;
	}

	public function enrollOldStudent(){
		$this->outputData = $this->changeStudentClassification("Old");
		return $this;
	}

	public function expelStudent(){
		$this->outputData = $this->changeStudentClassification("Expelled");
		return $this;
		
	}

	public function dropStudent(){
		$this->outputData = $this->changeStudentClassification("Dropped");
		return $this;	
	}


}