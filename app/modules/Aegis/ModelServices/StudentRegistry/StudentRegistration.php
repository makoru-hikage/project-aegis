<?php

namespace DeltaX\Aegis\ModelServices\StudentRegistry;

use DeltaX\Crud\ModelService\ModelService;
use DeltaX\Aegis\StudentRegistry\{
	NullStudentNumberException,
	NullUserNameException,
	NullDegreeException
};

class StudentRegistration extends ModelService {
	
	/**
	 * The student user involved, the enrollee
	 * 
	 * @var \DeltaX\Models\User
	 */
	protected $user;

	/**
	 * The degree program the student wants to enroll
	 * 
	 * @var \DeltaX\Models\Degree
	 */
	protected $degree;

	/**
	 * The student's other details
	 * 
	 * @var array
	 */
	protected $studentAttributes = [
		'date_of_enrollment' => null,
		'last_school_attended' => null,
		'classification' => null
	];

	/**
	 * The student's user details see User Model
	 * 
	 * @var array
	 */
	protected $userAttributes = [];

	/**
	 * Assign a degree program
	 * 
	 * @param string
	 */
	public function setDegree($degreeCode){
		$this->degree = $this->repoInvoker->find('degrees', $degreeCode);

		if (empty($degree)) {
			throw new NullDegreeException;
		}

		return $this;
	}

	/**
	 * Assign the user by username, it will be created
	 * if it is non-existent.
	 * 
	 * @param string
	 */
	public function setUser($username){
		$params = $this->userAttributes;
		$this->user = $this->repoInvoker->updateOrCreate('user', $username, $params);

		return $this;
	}

	/**
	 * Set the student attributes
	 * 
	 * @param array
	 */
	public function setStudentAttributes($attributes){

		//Force the process to have a student_number
		if ( empty($studentAttributes['student_number']) ){
			throw new NullStudentNumberException;
		}

		$this->studentAttributes = $attributes;

		return $this;
	}

	/**
	 * Checks if user exists
	 * 
	 * @param  string
	 * @return \DeltaX\Models\User|null
	 */
	public static function studentExists($studentNumber){
		return $this->repoInvoker->find('students', $studentNumber);
	}

	/**
	 * Create the student account using the supplied properties
	 * 
	 * @return \DeltaX\Models\Student
	 */
	public function createStudentAccount(){
		$params = $this->studentAttributes;
		$params['user_id'] = $this->user->id;
		$params['degree_id'] = $this->degree->id;

		$student = $this->repoInvoker->create('students', $params);

		$this->outputData = $student;
		return $this;
	}

}