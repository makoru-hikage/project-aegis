<?php

namespace DeltaX\Aegis\ValidationRules;

use Respect\Validation\Validator as v;
use DeltaX\Crud\InputValidation\ValidationSet;
use DeltaX\Crud\InputValidation\RespectValidationAdapter as rva;

class ChecklistSearchValidation extends ValidationSet {
	
	protected function loadRules(){

		$canBeIntOrRangeOfInt = rva::use(
			v::optional(
				v::when(
					v::arrayType(),
					v::arrayVal()
						->each(v::intVal())
						->length(2, 2),
					v::intVal()
				)
			)
		);

		$canBeNumOrRangeOfNum = rva::use(
			v::optional(
				v::when(
					v::arrayType(),
					v::arrayVal()->each(v::intVal())->length(2, 2),
					v::intVal()
				)
			)
		);

		$optionalString = rva::use(v::optional(v::stringType()));

		$optionalInt = rva::use(v::optional(v::intVal()));

		$this->validationRules = [
			'degree_code' => rva::use(v::stringType()),
			'course_name' => $optionalString,
			'course_code' => $optionalString,
			'year_level' => $canBeIntOrRangeOfInt,
			'division_type' => rva::use(v::in(['semester', 'trimester', 'quarter', 'summer', 'ojt'])),
			'part_number' => $canBeIntOrRangeOfInt,
			'credits_in_lecture' => $canBeIntOrRangeOfInt,
			'credits_in_lab' => $canBeIntOrRangeOfInt,
			'total_credits' => $canBeIntOrRangeOfInt,
			'lec_contact_hrs' => $canBeNumOrRangeOfNum,
			'lab_contact_hrs' => $canBeNumOrRangeOfNum,
			'final_grade' => $canBeNumOrRangeOfNum,
			'page' => $optionalInt,
			'perPage' => $optionalInt,
			'sort' => rva::use(v::optional(v::arrayType()))
		];

		return $this;
	}

}