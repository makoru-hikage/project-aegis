<?php


use \Slim\Views\{Twig, TwigExtension};

use \Symfony\Bridge\Twig\Extension\{FormExtension, TranslationExtension};
use \Symfony\Bridge\Twig\Form\{TwigRenderer, TwigRendererEngine};

use \Symfony\Component\Translation\Translator;
use \Symfony\Component\Translation\Loader\XliffFileLoader;

use \Symfony\Component\Security\Csrf\TokenStorage\NativeSessionTokenStorage;
use \Symfony\Component\Security\Csrf\TokenGenerator\UriSafeTokenGenerator;
use \Symfony\Component\Security\Csrf\CsrfTokenManager;

/* Flash Message: Userful for displaying errors */
    $container['flash'] = function () {
        return new FlashMessages();
    };



/* Render HTML forms: Allows Twig to render forms */
    // the Twig file that holds all the default markup for rendering forms
    $defaultFormTheme = 'bootstrap_3_horizontal_layout.html.twig';
    
	//Form Engine is the one that ties the Twig Environment and the Form Factory together
	$container['form_engine'] = new TwigRendererEngine([$defaultFormTheme]);
    $container->form_engine->setEnvironment($container->view->getEnvironment());

    //Preparing Form Extension for CSRF protection 
    $csrfGenerator = new UriSafeTokenGenerator();
    $csrfStorage = new NativeSessionTokenStorage('\Symfony\Component\HttpFoundation\Session\Session');
    $csrfManager = new CsrfTokenManager($csrfGenerator, $csrfStorage);

    $container['view']->addExtension(
        new FormExtension(new TwigRenderer($container->form_engine, $csrfManager))
    );

/* Translator */
    // NOTICE: To be edited when translation feature is needed.
    // Removing this snippet shall render the Symfony Form Factory's twig file erroneous
    $translator = new Translator('en');
    // add the TranslationExtension (gives us 'trans' and 'transChoice' filters)
    $container->view->addExtension(new TranslationExtension($translator));

/* Needed to render the Menu done by MenuManager */
    //This is the main menu manager.
    $container['menu_manager'] = function() use ($container){
            return new \SlimStarter\Menu\MenuManager($container);
    };

    //It shall probably be used to render the menu sidebar.
    $container->view->addExtension(new \SlimStarter\TwigExtension\MenuRenderer($container->menu_manager));

    //Creation of the Main Menu sidebar itself.
    $mainMenu = $container->menu_manager->create('main_sidebar');

        /** The most necessary menu item of the Main Sidebar */
        $dashboard = $mainMenu->createItem('dashboard', array(
            'label' => 'Dashboard',
            'icon'  => 'dashboard',
            'url'   => '/dashboard'
        ));

        $mainMenu->addItem('dashboard', $dashboard);
        $mainMenu->setActiveMenu('dashboard');

/** ============================================================== */