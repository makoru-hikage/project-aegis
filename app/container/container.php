<?php

use \Slim\Views\{Twig, TwigExtension};
use \Monolog\Logger;
use \Monolog\Handler\StreamHandler;
use \Illuminate\Database\Capsule\Manager as CapsuleManager;
use \Slim\Flash\Messages;
use \Cartalyst\Sentinel\Native\Facades\Sentinel;
use DeltaX\UserAuthentication\SentinelAuthenticationAdapter;

/* Container is made */
    $container = $app->getContainer();

/* Logs: Prepares logs */
    $container['logger'] = function($c) {
        $logger = new Logger('my_logger');
        $file_handler = new StreamHandler("../logs/app.log");
        $logger->pushHandler($file_handler);
        return $logger;
    };

/* Model: Activates Eloquent, an ORM. Prepares database connection */
    $capsule = new CapsuleManager;
    $capsule->addConnection($container->get('settings')['db']['connections']['mysql']);
    $capsule->setAsGlobal();
    $capsule->bootEloquent();
    $container['db_connection'] = $capsule->getConnection();

/* View: Prepares Twig for rendering of templates. */

    //Container is prepared for Twig, set 'debug' to 'false' in production mode.'
    $container['view'] = function ($container){
        $view = new Twig(
            ['../app/views', '../vendor/symfony/twig-bridge/Resources/views/Form'], 
            ['cache' => '../storage/cache',
            'debug' => 'false']);
        $view->addExtension(new TwigExtension(
            $container['router'],
            $container['request']->getUri()
        ));

        return $view;
    };

/* User Authentication: Activating Sentinel--Requires Eloquent */
    $sentinel = (new Sentinel())->getSentinel();
    $sentinel->getUserRepository()->setModel('DeltaX\Models\User');
    $sentinel->getRoleRepository()->setModel('DeltaX\Models\Role');;


    $container['auth'] = new SentinelAuthenticationAdapter($sentinel);

    require '../app/container/frontend_utils.php';





    
   
